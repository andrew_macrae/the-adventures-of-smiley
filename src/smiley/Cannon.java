/*
 *  This software is free.
 */

package smiley;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 */
public class Cannon extends Enemy
{
    private SpriteManager sMan_r, sMan_l, sMan_R, sMan_L;
    public char state;
    private int blastCount = 0;
    private Image ballImg,legsImg;
    private int ballW = 30;
    private double fireVel;
    private double theta = Math.PI/4;

    public Cannon(double xs, double ys, double ws, double hs, String sPath, double g)
    {
        super(xs, ys, ws, hs, g);
        try
        {
        ballImg  = ImageIO.read(new File("Images/Sprites/cannonball.png"));
        } catch (IOException ex)
        {
            Logger.getLogger(Enemy.class.getName()).log(Level.SEVERE, null, ex);
        }
        try
        {
            legsImg  = ImageIO.read(new File("Images/Sprites/cannon_legs.png"));
        } catch (IOException ex)
        {
            Logger.getLogger(Enemy.class.getName()).log(Level.SEVERE, null, ex);
        }
        bList = new BuList(5,ballImg);
        fireCounter = 0;
        fireDeadTime = 150;
        alive = true;
        type = 'r';
        dCy=-4;

        sMan_r = new SpriteManager(sPath + "cannon_barrel.png", 45, 45, 1, true);
        sMan_l = new SpriteManager(sPath + "cannon_barrel.png", 45, 45, 1, true);
        sMan_R = new SpriteManager(sPath + "cannon_barrel_fire.png", 45, 45, 6, false); // firing right
        sMan_L = new SpriteManager(sPath + "cannon_barrel_fire.png", 45, 45, 6, false); // firing left
        
        
        
        state = 'l';
        hitPoints = maxHitPoints= 5;
        hpCnt = 60;
        
        vx = 0;
        fireVel = 16;
    }

    public boolean addImage(String fN)
    {
        return false;
    }

    public void draw(Graphics g, double x0, double y0)
    {
        int dy = 4;
        Image toDraw = null;
        switch (state)
        {
            case 'r':
                toDraw = sMan_r.get();               
                break;
            case 'l':
                toDraw = sMan_l.get();
                break;
            case 'R':
                toDraw = sMan_R.get();
                break;
            case 'L':
                toDraw = sMan_L.get();
                break;
        }
        bList.drawAll(g,x0,y0);
        Graphics2D g2D = (Graphics2D) g;
        int xR = (int)(x-x0 + 17);
        int yR = (int)(y-y0 + 27);
        
        g2D.rotate((theta+Math.PI/4), xR,yR);
        g2D.drawImage(toDraw, (int) (x - x0), (int) (y - y0+dy), (int) w, (int) h, null);
        g2D.rotate(-(theta+Math.PI/4), xR, yR);
        g.drawImage(legsImg, (int) (x - x0), (int) (y - y0+dy), (int) w, (int) h, null);
        
        drawHP(g,(int)x0,(int)y0);
    }

    public void iterate(double d, boolean collH, boolean collV, boolean willFall, boolean onSlope, double dVX, double dVY)
    {
        super.iterate(d, collH, collV, willFall, onSlope, dVX, dVY);
        if(fireCounter>0) fireCounter--; 
        if(blastCount>0) blastCount--; 
        bList.iterate(d);
        if(playerVicinity<750&&fireCounter<1 && true)
        {
            sMan_R.resetCurrFrame();
            sMan_R.resetCurrFrame();
            blastCount = 90;
            fireCounter = fireDeadTime;
            state = Character.toUpperCase(type);
            theta = getStaticTheta(sign(Math.random()*10-5));
        }
        
        switch (state)
        {
            
            case 'r':
                sMan_r.iterate(d);
                break;
            case 'l':
                sMan_l.iterate(d);
                break;
            case 'R':
                sMan_R.iterate(d);
                if(blastCount==30) 
                {
                    bList.add(new CannonBall(x+.2*w,y+0.3*h,ballW,ballW,fireVel*Math.cos(theta),fireVel*Math.sin(theta),grav));
                    MainPanel.clips.CANNON_FIRE.play();
                }
                
                if(blastCount<=0) state = 'r';
                break;
            case 'L':
                sMan_L.iterate(d);
                if(blastCount==30) bList.add(new CannonBall(x+0.6*w,y-0.2*h,ballW,ballW,fireVel*Math.cos(theta),Math.sin(theta),grav));
                if(blastCount<=0) state = 'l';                
                break;
        }
    }
    
    public boolean setState(char c)
    {
        if (c == 'l')
        {
            state = 'l';
            return true;
        } else if (c == 'r')
        {
            state = 'r';
            return true;
        } else
        {
            return false;
        }
    }

    public void kill()
    {
//        hitPoints--;
//        if(hitPoints<=0)
//        {
//            alive = false;
//            if(state == 'r')
//            {
//                state = 'd';
//            }
//            else state = 'D';
//        }
//        else
//        {
//            hpCounter = hpCnt;
//        }
    }
    
    private double getStaticTheta(int sgn)
    {
        double a1 = (grav*(playerX-x))/(fireVel*fireVel);
        double a2 = 2*a1*(playerY-y)/(playerX-x);
        
        if(1+a2-Math.pow(a1,2)<0)
        {
            return -Math.PI/4;
        }
        else
        {
            double z = -Math.atan((1+sgn*Math.sqrt(1+a2-Math.pow(a1,2)))/a1);
            if(playerX<x) z+=Math.PI;
            return z;
        }
    }
    class CannonBall extends Projectile
    {
        public CannonBall(double xs, double ys, double ws, double hs,double vvx, double vvy,double gr)
        {
            super(xs,ys,ws,hs);
            lifeTime = 200;
            vx =vvx;
            vy = vvy;
            grav = gr;
        }
        
        @Override
        public void iterate(double d)
        {
            x+=d*this.vx;
            y+=d*vy;
            this.vy+=grav*d;
            
            lifeTime--;
        }
        @Override
        public void draw(Graphics g, double x0, double y0)
        {
            g.fillOval((int)(x-x0),(int)(y-y0),(int)w,(int)h);
        }
    }
}

