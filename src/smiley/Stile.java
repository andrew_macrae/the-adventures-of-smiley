/*
 *  This software is free.
 */

package smiley;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Line2D;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 */

public class Stile extends Tile
{

    public double lT, rT;
    public int[] xPts =
    {
        1, 1, 1, 1, 1
    };
    public int[] yPts;

    double cos, sin, tan;

    public Stile(double xs, double ys, double ws, double hs, double lTs, double rTs,int g, int im)
    {
        super(xs, ys, ws, hs,g,im);
        lT = lTs;
        rT = rTs;
        int xPts0[] =
        {
            (int) x, (int) x, (int) (x + w), (int) (x + w), (int) x
        };
        int yPts0[] =
        {
            (int) (y + h), (int) (y + lT), (int) (y + rT), (int) (y + h), (int) (y + h)
        };
        xPts = xPts0;
        yPts = yPts0;
        double hyp = Math.sqrt((rT - lT) * (rT - lT) + w * w);
        sin = (rT - lT) / hyp;
        cos = w / hyp;
        tan = sin / cos;
        
        if(xPts[0]==xPts[1]) collMap[0] = false;
        if(xPts[2]==xPts[3]) collMap[2] = false;
    }
    
    public void draw(Graphics g, double x0, double y0, Image im)
    {
        g.drawImage(im,(int)(x-x0),(int)(y-y0),(int)w,(int)h,null);
        if(drawDebug)
        {
            g.setColor(Color.yellow);
            Graphics2D g2 = (Graphics2D) g;
            g2.draw(new Line2D.Double(xPts[0]-x0, yPts[0]-y0, xPts[1]-x0, yPts[1]-y0));
            g2.draw(new Line2D.Double(xPts[1]-x0, yPts[1]-y0, xPts[2]-x0, yPts[2]-y0));
            g2.draw(new Line2D.Double(xPts[2]-x0, yPts[2]-y0, xPts[3]-x0, yPts[3]-y0));
            g2.draw(new Line2D.Double(xPts[3]-x0, yPts[3]-y0, xPts[0]-x0, yPts[0]-y0));
            g.setColor(Color.green);
            g2.setStroke(new BasicStroke(2));
            if (collMap[0])
            {
                g2.draw(new Line2D.Double(xPts[0]-x0, yPts[0]-y0, xPts[1]-x0, yPts[1]-y0));
            }
            if (collMap[1])
            {
                g2.draw(new Line2D.Double(xPts[1]-x0, yPts[1]-y0, xPts[2]-x0, yPts[2]-y0));
            }
            if (collMap[2])
            {
                g2.draw(new Line2D.Double(xPts[2]-x0, yPts[2]-y0, xPts[3]-x0, yPts[3]-y0));
            }
            if (collMap[3])
            {
                g2.draw(new Line2D.Double(xPts[3]-x0, yPts[3]-y0, xPts[0]-x0, yPts[0]-y0));
            }
            drawDebug = false;
        }
    }

    @Override
    public boolean isIn(Squob s, double dx, double dy)
    {
        if ((s.x + s.dCx + s.dCw + dx > x + dCx) && (s.x + s.dCx + dx < x + dCx + dCw) && (s.y + s.dCh + dy > y + dCy) && (s.y + s.dCy + dy < y + dCy + dCh))
        {// Only way for above statent to be true and have no collision is if squob is above and to right
            if (((y + lT) + ((s.x - x) + s.dCx) * tan > s.y + s.h + dy) && ((y + lT) + ((s.x - x) + s.dCx + s.dCw) * tan > s.y + s.h + dy))
            {
                return false;
            } else
            {
                return true;
            }
        } else
        {
            return false;
        }
    }

    public void copy(Stile s)
    {
        x = s.x;
        y = s.y;
        w = s.w;
        h = s.h;
        dCx = s.dCx;
        dCy = s.dCy;
        dCw = s.dCw;
        dCh = s.dCh;
        type = s.type;
        imgHash = s.imgHash;
        xPts = s.xPts;
        yPts = s.yPts;
        rT = s.rT;
        lT = s.lT;
        sin = s.sin;
        cos = s.cos;
        tan = s.tan;
    }
//Boolean isIs() should be a quick check. Then if, and only if there is a collision, do this.

    public V2 getNormal(Squob s, double dx, double dy) // return vector perpendicular to colliding surface
    {
        // If last step above, current step not -> top side        
        double tx = 0, ty = 0;
        double xL = s.x + s.dCx;
        double xR = s.x + s.dCx + s.dCw;
//        if((xL<w&&xL>0&&(y+lT+xL*tan<s.y+s.dCh))||xR>0&&xR<w&&(y+lT+xR*tan<s.y+s.dCh))
//        if(s.y+s.h<Math.max(y+lT,y+rT))
        if (isCollLineSegment(new LineSeg(xPts[1], yPts[1], xPts[2], yPts[2]), new LineSeg(s.x + dx + s.dCx, s.y + s.dCh + s.dCy + dy, s.x + dx + s.dCx + s.dCw, s.y + s.dCh + s.dCy + dy)))
        { // TOP            
            tx = xPts[2] - xPts[1];
            ty = yPts[2] - yPts[1];
            sides[1] = 1;
        } // If last step below, current step not -> bottom side
        else if ((s.y > yPts[0]) && (s.y + dy < yPts[0]))
        { // BOTTOM
            tx = xPts[4] - xPts[3];
            ty = yPts[4] - yPts[3];
            sides[3] = 1;
        } else if (s.x + dx < xPts[0])
        { // LEFT
            tx = xPts[1] - xPts[0];
            ty = yPts[1] - yPts[0];
            sides[0] = 1;
        } else
        { // RIGHT
            tx = xPts[3] - xPts[2];
            ty = yPts[3] - yPts[2];
            sides[2] = 1;
        }
        return new V2(tx, ty).perp();
    }

    public V2 getSurface()
    {
        return new V2(xPts[2] - xPts[1], yPts[2] - yPts[1]).unit();
    }
    public LineSeg getSurfaceLineSeg()
    {
        return new LineSeg(xPts[1], yPts[1], xPts[2], yPts[2]);
    }
    @Override
    public V2 getSurfaceNorm(int idx)
    {
        switch(idx)
        {
            case 0:
                return new V2(-1,0);
            case 1:
                return new V2(xPts[2] - xPts[1], yPts[2] - yPts[1]).perp();
            case 2:
                return new V2(1,0);
            case 3:
                return new V2(0,1);
            default:
                return new V2(0,0);
        }
    }

    public boolean[] getSide(Squob s, double dx, double dy)
    { //[l,u,r,d]
        boolean[] ret =
        {
            false, false, false, false
        };
        double sXL = s.x + s.dCx + dx;
        double sXR = s.x + s.dCx + s.dCw + dx;
        double sYU = s.y + dy + s.dCy;
        double sYD = sYU + s.dCh;

        if (isCollLineSegment(new LineSeg(xPts[1], yPts[1], xPts[2], yPts[2]), new LineSeg(s.x + dx + s.dCx, sYD, s.x + dx + s.dCx + s.dCw, sYD)))
        {//top
            ret[1] = collMap[1];
        }
        if (sXL < x + w && sXR > x + w && sYD > yPts[2] && sYU < yPts[3])
        {// right
            ret[2] = collMap[2];
        }
        if (sYU < yPts[0] && sYD > yPts[0] && sXR > x && sXL < x + w)
        {//bottom
            ret[3] = collMap[3];
        }
        if (sXR > x && sXL < x && sYD > yPts[1] && sYU < yPts[0])
        {// left
            ret[0] = collMap[0];
        }
        //System.out.println("["+ret[0]+","+ret[1]+","+ret[2]+","+ret[3]+"]");
        return ret;
    }
    @Override
    public boolean isInFace(Squob s,double dx, double dy)
    { // We really only wants this for Stiles, precious.
        LineSeg l1 = new LineSeg(xPts[1],yPts[1],xPts[2],yPts[2]);
        LineSeg l2 = new LineSeg(s.x+s.dCx+s.dCw/2+dx,s.y+s.dCy+dy,s.x+s.dCx+s.dCw/2+dx,s.y+s.dCy+s.dCh+dy);
        return isCollLineSegment(l1,l2);
    }
    public String toString()
    {
        return "Stile: (x,y,w,h,tL,tR) = "+x+","+y+","+w+","+h+","+lT+","+rT+")";
    }
    @Override
    public double lockY(Squob s) // given squob with x-coord (s.x+s.dCx+s.dCw/2), return y s.t. y+dCy+dCy is on ground
    { // dx = Sprite center - x1. m = slope, y(dx) = y1 + m*dx
        return yPts[1] + (s.x+s.dCx+s.dCw/2 - xPts[1])*(yPts[2]-yPts[1])/(xPts[2]-xPts[1])-s.dCh-s.dCy;
    }    
    
    public double getCos()
    {
        return getSurfaceLineSeg().length()/getSurfaceLineSeg().width();
    }
    public double getSin()
    {
        return getSurfaceLineSeg().height()/getSurfaceLineSeg().width();
    }    
}