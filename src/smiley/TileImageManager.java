package smiley;

import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * @author Andrew MacRae <macrae@berkeley.edu>
 * This is a temporary data structure used during the loading of a level:
 * the map files consist of sprite sheets, embedded with numerous tiles.
 * The purpose here is to produce tiles containing a single image from this.
 */
public class TileImageManager 
{
    private TileImagePoint[] tp;
    private int max;
    private int sz;
    public TileImageManager(int max)
    {
        tp = new TileImagePoint[max];
        sz = 0;
    }
    public void add(BufferedImage im,int startGID,int nTiles, int w, int h)
    {
        for(int i = 0;i<nTiles;i++)// for now assume 1D array
        {
            Image tmpImg = im.getSubimage(i*w, 0, w, h);
            tp[startGID+i] = new TileImagePoint(tmpImg,w,h,startGID+i);
        }
        sz+=nTiles;
    }
    public Image get(int id)
    {
        return tp[id].getImage();
    }
    public TileImagePoint getTileImage(int id)
    {
        return tp[id];
    }
    public int getSize()
    {
        return sz;
    }    
}