/*
 * Currently: level loading is broken. Fix it!
 * 
 * Major change: Squobs no longer hold an Image, but just a key - DONE -
 * Next: Drawing position of background images - DONE - 
 * LinkedList nullPointer fix (try-catch?)
 * Next, make camera follow smoothly
 * Wall jumps
 */
package smiley;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author amacrae
 */
public class MainPanel extends JPanel implements Runnable
{
// new variables to be placed
    private int cameraTolLR;
    private int cameraTolU;
    private int cameraTolD;
// ******* Globals
// keep track of global time
    private long gClock = System.currentTimeMillis();
    private boolean gClockEnabled = false;
    private boolean drawCamera = true;
    
    // game states

    public int TEMPSC = 0, sCC = 0;
    public int[] cameraLock = {0,0,0,0}; // whether camera is locked [west,north,east,south}

    private int gameState, level;
    public static final int INTRO = 0, PLAYING = 1, CUTSCENE = 2, GAMEOVER = 3,
            JUSTDIED = 4, LOADINTRO = 5, LOADLEVEL = 6, BOSS = 7, PASSEDBOSS = 8, 
            WON = 9, BOSSTRANSITION = 10;

    private boolean left, right, up, down; // keyboard status booleans
    private boolean jump, ground; // Is player jumping/grounded?

//    private V2 pos, vel; // do we need this here? Or do we merely care about sprite.pos?
    private double accel, bounce;

    private boolean sound = false;

// game physics
    private double grav, gammaAir, muS, muD; // gravity, air resistance, static&dynamic friction coeeficients.
// Map poperties
    private int W, H;
    private int mapW, mapH; // level file is mapW x mapH tile array
    private double hScale, vScale;
    private int mapTileW, mapTileH; // with each tile having these dimensions
    private int mapGridSize;
    private int spriteGID; // character code of the first sprite in the level file
    private int numEnemies;
    private int bossIndex;
    private double bossXL, bossXR, bossYU, bossYD;
    private double levelMinX, levelMaxX, levelMinY, levelMaxY; // Holds the max/min indices of the level
    private int slopeStartIdx, slopeStopIdx, slopeDelta, numSlopes;// slope delta is the distance between the first Tile and the first Stile

// Map Objects
    private TileLayer tiles, stiles, fg1, bg1, bg2, bg3, spikes;
    private TileImageManager tm;
    private int levelMax;
// Threading, game loop variables 
    private Thread fred;
    private int sleepTime;
    private int tickCounter, invincibleTimer;
// File references
    private String levelPath, spritePath, screenPath, soundPath;
// Player/NPC stuff
    private Player sp;
    private double playerStartX, playerStartY;
    private Enemy[] enemies;
    private int pw, ph; // player width/height
    private int lives;
    private int playerHP, playerMaxHP;
// Screens
    private Image introScreen, gameOverScreen, loadScreen, level1Screen, level2Screen, youWinScreen;
    private Image x0,x1,x2,x3,hBarImg,hBarBossImg,sHatImg,bossBustImg;
// Graphics
    private Graphics2D g2D;
    private GradientPaint skye;
    private Color gnd;
    private int skyeY;
    private double bossScreenX, bossScreenY;
    private double CX, CY, CW, CH;
    private double screenX, screenY, tmpScreenX;
    private int hBarX, hBarY, hBarW, hBarH;
    private int hBarXBoss, hBarYBoss, hBarWBoss, hBarHBoss;
    private int x1HP,x2HP,x3HP,x4HP,y1HP,y2HP,y3HP;
    private int x1HPBoss,x2HPBoss,x3HPBoss,x4HPBoss,y1HPBoss,y2HPBoss,y3HPBoss;
    private int[] xPsHP;
    private int[] yPsHP;
    private int[] xPsHPBoss;
    private int[] yPsHPBoss;
//Sound
    private MidiPlayer mPlayer;
    public static ClipPlayer clips;

    public MainPanel()
    {
        init();
    }

    public void init()
    {
        W = 900;
        H = 600;
        CX = W / 2;
        CY = H / 2;
        cameraTolLR = 100;
        cameraTolU = 135;
        cameraTolD = 65;

        left = right = up = down = jump = ground = false;

        mapTileW = mapTileH = 30;
        vScale = hScale = 1.0;
        pw = mapTileW;
        ph = (3 * mapTileH) / 2;

        CW = 4*mapTileW;
        CH = 7*mapTileW;
        lives = 2;
        levelMax = 2;
        playerHP = playerMaxHP = 5;
// These coordinates define the players Health bar. See Documentation/hpLegend.png for details.
        hBarX = (int)(25*hScale);
        hBarY = (int)(5*hScale);
        hBarW = (int)(154*hScale);
        hBarH = (int)(28*hScale);
        
        x1HP = hBarX;
        x2HP = hBarX + (hBarW * 32) / 200;
        x3HP = hBarX + (hBarW * 64) / 200;
        x4HP = hBarX + hBarW;
        y1HP = hBarY+2;
        y2HP = hBarY + (hBarH * 22) / 44 + 2;
        y3HP = hBarY + hBarH-2;
// Boss's hp Bar x-coords are the same, but reflected about the center of the screen
        hBarHBoss = hBarH;
        hBarWBoss = hBarW;
        x1HPBoss = W - x4HP;
        x2HPBoss = W - x3HP;
        x3HPBoss = W - x2HP;
        x4HPBoss = W - x1HP;
        y1HPBoss = y1HP;
        y2HPBoss = y2HP;
        y3HPBoss = y3HP;
        int[] tx =
        {
            x2HP, x2HP, x3HP, x3HP, x2HP
        };
        int[] ty =
        {
            y3HP, y1HP, y1HP, y2HP, y3HP
        };
        int[] txBoss =
        {
            x3HPBoss, x3HPBoss, x2HPBoss, x2HPBoss, x3HPBoss
        };
        int[] tyBoss =
        {
            y3HPBoss, y1HPBoss, y1HPBoss, y2HPBoss, y3HPBoss
        };
        xPsHP = tx;
        yPsHP = ty;
        xPsHPBoss = txBoss;
        yPsHPBoss = tyBoss;

        gameState = LOADINTRO;
        sleepTime = 10;
        tickCounter = 0;

        grav = 0.4;     // gravity
        gammaAir = 0.05*hScale; // air resistance
        muS = 0.05*hScale;
        muD = 0.2*hScale;
        accel = 0.5*hScale*hScale;    // left/right acceleration
        bounce = 15*hScale*hScale;   // jumping power

        level = 1;
        slopeDelta = 15;
        numSlopes = 6;

        screenPath = "Images/Screens/";
        spritePath = "Images/Sprites/";
        levelPath = "Levels/";
        soundPath = "Sounds/";
        
        mPlayer = new MidiPlayer(soundPath + "Level3.mid", true);

        try
        {
            introScreen = ImageIO.read(new File(screenPath + "Mark1.png"));
            loadScreen = ImageIO.read(new File(screenPath + "LoadMark1.png"));
            level1Screen = ImageIO.read(new File(screenPath + "level1.png"));
            level2Screen = ImageIO.read(new File(screenPath + "level2.png"));
            gameOverScreen = ImageIO.read(new File(screenPath + "GameOverMark1.png"));
            youWinScreen = ImageIO.read(new File(screenPath + "YouWin.png"));
            hBarImg = ImageIO.read(new File(screenPath + "hpBar_trans.png"));
            hBarBossImg = ImageIO.read(new File(screenPath + "hpBar_rev.png"));
            sHatImg = ImageIO.read(new File(screenPath + "smileyHat.png"));
            bossBustImg = ImageIO.read(new File(screenPath + "bossBust.png"));
            x0 = ImageIO.read(new File(screenPath + "Fonts/x0.png"));
            x1 = ImageIO.read(new File(screenPath + "Fonts/x1.png"));
            x2 = ImageIO.read(new File(screenPath + "Fonts/x2.png"));
            x3 = ImageIO.read(new File(screenPath + "Fonts/x3.png"));
        } 
        catch (IOException e)
        {
            System.out.println("Error loading screen images");
            e.printStackTrace();
        }

        resetTiles();

        fred = new Thread(this);
        fred.start();
    }
    /* ***************************************************************************
     *   ************************** Event Loop  ********************************
     * ************************************************************************** */

    public void start()
    {

    }

    public void run()
    {
        while (true)
        {
            if (gameState == LOADINTRO)
            {

                loadPlayer();
                loadLevel(levelPath + "level1.tmx");
                System.out.println("Size of (tiles,stiles): (" + tiles.size() + "," + stiles.size() + ")");
                levelMinX = levelMinY = 0;
                levelMaxX = (int) tiles.get(tiles.size() - 1).x + mapTileW; // The map file could be much larger than the level itself
                levelMaxY = (int) tiles.get(tiles.size() - 1).y +  mapTileH;

                if (sound)
                {
                    mPlayer.setTrack(soundPath + "Level3.mid", true);
                    mPlayer.play();
                }
                gameState = INTRO;
                repaint();
            } else if (gameState == LOADLEVEL)
            {
                //              System.out.print("LOADLEVEL.\n");
                String lPath;
                switch (level)
                {
                    case 1:
                        skye = new GradientPaint(W / 2, H, Color.BLUE, W / 2, 0, Color.WHITE);
                        gnd = new Color(12, 134, 61);
                        skyeY = 1025;
                        lPath = levelPath + "level1.tmx";
                        bossScreenX = 70 * mapTileW;
                        bossScreenY = 11 * mapTileH;
                        bossXL = 61 * mapTileW;
                        bossXR = bossXL + 30 * mapTileW;
                        bossYU = 2 * mapTileH;
                        bossYD = bossYU + 15 * mapTileH;
                        if (sound)
                        {
                            mPlayer.setTrack(soundPath + "Level1.mid", true);
                            mPlayer.play();
                        }
                        break;
                    case 2:
                        skye = new GradientPaint(W / 2, H, Color.RED, W / 2, 0, Color.WHITE);
                        gnd = new Color(12, 134, 61);
                        skyeY = 1020;
                        lPath = levelPath + "level2.tmx";
                        bossScreenX = 70 * mapTileW;
                        bossScreenY = 11 * mapTileH;
                        bossXL = 61 * mapTileW;
                        bossXR = bossXL + 30 * mapTileW;
                        bossYU = 2 * mapTileH;
                        bossYD = bossYU + 15 * mapTileH;
                        if (sound)
                        {
                            mPlayer.setTrack(soundPath + "Level2.mid", true);
                            mPlayer.play();
                        }
                        break;
                    default:
                        skye = new GradientPaint(W / 2, H, Color.BLUE, W / 2, 0, Color.WHITE);
                        lPath = levelPath + "level1.tmx";
                        bossScreenX = 135 * mapTileW;
                        bossScreenY = 11 * mapTileH;
                        bossXL = 128 * mapTileW;
                        bossXR = bossXL + 30 * mapTileW;
                        bossYU = 11 * mapTileH;
                        bossYD = bossYU + 15 * mapTileH;
                        lPath = levelPath + "level1.tmx";
                        if (sound)
                        {
                            mPlayer.setTrack(soundPath + "Level1.mid", true);
                            mPlayer.play();
                        }
                        break;
                }
                double tt = System.currentTimeMillis();
                loadLevel(lPath);

                levelMinX = levelMinY = 0;
                levelMaxX = tiles.get(tiles.size() - 1).x + mapTileW;
                levelMaxY = tiles.get(tiles.size() - 1).y + mapTileH;

                System.out.print("\n ... done loading level in "+(System.currentTimeMillis()-tt)+"ms.\n");                
                try
                {
                    
                    Thread.sleep(200);
                    // Thread.sleep(200);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

                gameState = PLAYING;
                System.out.print("Playing.\n");
                repaint();
            } else if (gameState == PLAYING || gameState == BOSS)
            {
            // reset flags

                // apply acceleration according to input/physics
                sp.vy = sp.vy * (1 - gammaAir);
                sp.vx = sp.vx * (1 - muD);
                if (Math.abs(sp.vx) < muS / 100)
                {// static friction
                    sp.vx = 0;
                }
                if (left)
                {
                    sp.vx -= accel;
                }
                if (right)
                {
                    sp.vx += accel;
                }
                if (jump && ground)
                {
                    sp.vy -= bounce;
                    jump = false;
                    ground = false;
                }
                //ground = false;
                sp.vy += grav;

                // Now on to the NPCs
                // First check for player NPC collisions
                for (int i = 0; i < numEnemies; i++)
                {
                    if (enemies[i].isAlive())
                    {
                        enemies[i].setPlayerCoords(sp.x,sp.y,sp.vx,sp.vy);
                        if (enemies[i].isInWeapon(sp, sp.vx, sp.vy)&&!sp.invincible)
                        {
                            if(enemies[i].getId().equals("Spider Boss"))
                            {
                                sp.setSticky();
                            }
                            else
                            {
                                killPlayer();
                            }                            
                            break;                            
                        }
                        if (enemies[i].isIn(sp, sp.vx, 0)&&!sp.invincible)
                        {
                            killPlayer();
                            break;
                        }
                        if (enemies[i].isIn(sp, 0, sp.vy)&&!enemies[i].isIn(sp, 0, 0))
                        {
                            sp.vy = -2 * bounce / 3;
                            enemies[i].kill();
                            if (sound)
                            {
                                char c = enemies[i].getType();
                                if(enemies[i].isAlive()) c = 'B';
                                playClip(c);
                            }
                        }
                    }
                }

                handlePlayerCollisions();
                handleNPCCollisions();
                // update position based on velocity
                //if(sp.sticky) sp.vx/=10;
                sp.x += sp.vx;
                sp.y += sp.vy;

                setSpriteState();
                sp.iterate(Math.abs(sp.vx));
                // if players/on-screen-NPCs are stuck, get them out
                evacuate(sp);

                if (sp.invincible)
                {
                    invincibleTimer--;
                    if (invincibleTimer <= 0)
                    {
                        sp.invincible = false;
                    }
                }

                if (sp.x >= bossXL && sp.x < bossXR && gameState != BOSS && sp.y >= bossYU && sp.y < bossYD)
                {
                    gameState = BOSSTRANSITION;
                    enemies[bossIndex].state = 'u';
                    gnd = new Color(12, 134, 61);
                    skyeY = 1020;
                    tickCounter = 255;
                    if (sound)
                    {
                        mPlayer.setTrack(soundPath + "boss.mid", true);
                        mPlayer.play();
                    }
                }
                if (!enemies[bossIndex].isAlive())
                {
                    level++;
                    gameState = PASSEDBOSS;
                    enemies[bossIndex].y+=6;
                    tickCounter = 250;
                    skye = new GradientPaint(W / 2, H, Color.YELLOW, W / 2, 0, Color.WHITE);
                    if (sound)
                    {
                        mPlayer.setTrack(soundPath + "passedLevel.mid", false);
                        mPlayer.play();
                    }
                }
            }
            else if (gameState == BOSSTRANSITION)
            {
                tickCounter--;
                skye = new GradientPaint(W / 2, H, Color.BLUE, W / 2, 0, new Color(tickCounter,tickCounter,tickCounter));
                if(tickCounter<1) 
                {
                    gameState = BOSS;
                    enemies[bossIndex].state = 'w';
                }
                enemies[bossIndex].iterate(0.5, false, true, false, false, 0, 0);
            }
            else if (gameState == JUSTDIED)
            {
                if (tickCounter > 0)
                {
                    tickCounter--;
                } else
                {
                    if (sound)
                    {
                        if (level == 1)
                        {
                            mPlayer.setTrack(soundPath + "Level1.mid", true);
                        }
                        if (level == 2)
                        {
                            mPlayer.setTrack(soundPath + "Level2.mid", true);
                        }
                        mPlayer.play();
                    }
                    gameState = PLAYING;
                    sp.x = playerStartX;
                    sp.y = playerStartY;
                    sp.vx = sp.vy = 0;
                    jump = false;
                }
                repaint();
            } else if (gameState == PASSEDBOSS)
            {
                if (tickCounter > 0)
                {
                    tickCounter--;
                    enemies[bossIndex].iterate(0.15, false, true, false, false, 0, 0);
                } else
                {
                    if (level <= levelMax)
                    {
                        gameState = LOADLEVEL;
                        sp.x = playerStartX;
                        sp.y = playerStartY;
                        sp.vx = sp.vy = 0;
                        jump = false;
                    } else
                    {
                        tickCounter = 300;
                        gameState = WON;
                        if (sound)
                        {
                            mPlayer.setTrack(soundPath + "Level3.mid", true);
                            mPlayer.play();
                        }
                    }
                }
                repaint();
            }
            else if (gameState == GAMEOVER)
            {
                if (tickCounter > 0)
                {
                    tickCounter--;
                } else
                {
                    if (sound)
                    {
                        mPlayer.setTrack(soundPath + "Level3.mid", true);
                        mPlayer.play();
                    }
                    loadLevel(levelPath + "level1.tmx");
                    gameState = INTRO;
                    lives = 2;
                }
                repaint();
            } else if (gameState == WON)
            {
                if (tickCounter > 0)
                {
                    tickCounter--;
                } else
                {
                    loadLevel(levelPath + "level1.tmx");
                    gameState = INTRO;
                    lives = 2;
                    level = 1;
                }
                repaint();
            }

            try
            {
                Thread.sleep(sleepTime);
//                System.out.print(".");
                repaint();
            } catch (InterruptedException e)
            {
                System.out.println(e.toString());
            }
        }
    }

    private void killPlayer()
    {
        playerHP--;
        if (playerHP >= 0)
        {
            if(sound)
            {
                clips.PLAYER_HIT.play();
            }
            sp.invincible = true;
            invincibleTimer = 200;
            sp.y -= 3;
            sp.vy = - 5;
            sp.vx = -2 * sp.vx;
            // Do player getting hit/invincibility here
        } else
        {
            if (sound)
            {
                mPlayer.stop();
                clips.DIE.play();
                System.out.println("DIE!");
            }
            lives--;
            playerHP = playerMaxHP;
            sp.x = playerStartX;
            sp.y = playerStartY;
       // Should replace with resetBoss() or enemies[bossIndex].reset()   
            enemies[bossIndex].state = 'a';
            enemies[bossIndex].vx= 0;
            enemies[bossIndex].vy= 0;
            if (lives >= 0)
            {
                gameState = JUSTDIED;
                tickCounter = 250;
            } else
            {
                gameState = GAMEOVER;
                level = 1;
                tickCounter = 400;
                if (sound)
                {
                    mPlayer.setTrack(soundPath + "gameOver.mid", false);
                    mPlayer.play();
                }
            }
        }
    }

    /* ***************************************************************************
     *   ************************** Drawing/Sound*******************************
     * ************************************************************************** */
    @SuppressWarnings("override")
    public void paintComponent(Graphics g)
    {
        if (gameState == LOADINTRO)
        {
            g.drawImage(loadScreen, 0, 0, W, H, null);

        } else if (gameState == INTRO)
        {
            g.drawImage(introScreen, 0, 0, W, H, null);
        } else if (gameState == LOADLEVEL)
        {
            Image toDraw = null;
            switch (level)
            {
                case 1:
                    toDraw = level1Screen;
                    break;
                case 2:
                    toDraw = level2Screen;
                    break;
            }
            g.drawImage(toDraw, 0, 0, W, H, null);
        } 
        else if (gameState == PLAYING)
        {
//            System.out.print("Entered ");
            g2D = (Graphics2D) g;
            g2D.setPaint(skye);
            g2D.fill(new Rectangle(0, 0, W, Math.max(skyeY - (int) screenY, 0)));
            g.setColor(gnd);
            updateCamera();
            g.fillRect(0, Math.max(skyeY - (int) screenY, 0), W, H - Math.max(skyeY - (int) screenY, 0));
 //           System.out.print("_bg3_");
            drawLayer(g, bg3, 4);
 //           System.out.print("_bg2_");
            drawLayer(g, bg2, 2);
 //           System.out.print("_bg1_");
            drawLayer(g, bg1, 1);
 //           System.out.print("_stiles_");
            drawLayer(g, stiles, 1);
 //           System.out.print("_tiles_");
            drawLayer(g, tiles, 1);
 //           System.out.print("_spikes_");
            drawLayer(g, spikes, 1);
 //           System.out.print("_done\n");
            if (!sp.invincible || ((invincibleTimer / 4) % 2 == 0))
            {
                sp.draw(g, screenX, screenY);
            }
            //
            g.setColor(Color.white);

            for (int i = 0; i < numEnemies; i++)
            {
                enemies[i].draw(g, screenX, screenY);
            }
            drawLayer(g, fg1, 1);

            drawPlayerStatus(g);
        } 
        else if (gameState == BOSS || gameState == BOSSTRANSITION)
        {
            g2D = (Graphics2D) g;
            g2D.setPaint(skye);
            g2D.fill(new Rectangle(0, 0, W, Math.max(skyeY - (int) screenY, 0)));
            g.setColor(gnd);
            g.fillRect(0, Math.max(skyeY - (int) screenY, 0), W, H - Math.max(skyeY - (int) screenY, 0));
            updateCamera();
            drawLayer(g, bg3, 6, bossScreenX, bossScreenY);
            drawLayer(g, bg2, 4, bossScreenX, bossScreenY);
            drawLayer(g, bg1, 1, bossScreenX, bossScreenY);
            drawLayer(g, stiles, 1, bossScreenX, bossScreenY);
            drawLayer(g, tiles, 1, bossScreenX, bossScreenY);
            drawLayer(g, spikes, 1, bossScreenX, bossScreenY);
//            g.fillRect((int)(currScreenX*mapTileW),(int)(currScreenY*mapTileH),(int)(CW),(int)(CH));
            if (!sp.invincible || ((invincibleTimer / 4) % 2 == 0))
            {
                sp.draw(g, bossScreenX - CX, bossScreenY - CY);
            }
            //
            g.setColor(Color.white);

            for (int i = 0; i < numEnemies; i++)
            {
                enemies[i].draw(g, bossScreenX, bossScreenY);
            }
            drawLayer(g, fg1, 1, bossScreenX, bossScreenY);
            
            if(gameState == BOSS)
            {
                drawPlayerStatus(g);
                drawBossStatus(g,enemies[bossIndex].getHP(),enemies[bossIndex].getMaxHP());
            }
        } else if (gameState == PASSEDBOSS)
        {
            g2D = (Graphics2D) g;
            g2D.setPaint(skye);
            g2D.fill(new Rectangle(0, 0, W, Math.max(skyeY - (int) screenY, 0)));
            g.setColor(gnd);
            g.fillRect(0, Math.max(skyeY - (int) screenY, 0), W, H - Math.max(skyeY - (int) screenY, 0));
            drawLayer(g, bg3, 6, bossScreenX, bossScreenY);
            drawLayer(g, bg2, 4, bossScreenX, bossScreenY);
            drawLayer(g, bg1, 1, bossScreenX, bossScreenY);
            drawLayer(g, tiles, 1, bossScreenX, bossScreenY);
            drawLayer(g, stiles, 1, bossScreenX, bossScreenY);
            drawLayer(g, spikes, 1, bossScreenX, bossScreenY);
            sp.draw(g, bossScreenX - CX, bossScreenY - CY);

            for (int i = 0; i < numEnemies; i++)
            {
                enemies[i].draw(g, bossScreenX, bossScreenY);
            }

        } else if (gameState == GAMEOVER)
        {
            g.drawImage(gameOverScreen, 0, 0, W, H, null);
        } else if (gameState == JUSTDIED)
        {
            if ((tickCounter / 8) % 2 == 0)
            {
                g.setColor(Color.black);
            } else
            {
                g.setColor(Color.red);
            }
            g.fillRect(0, 0, W, H);
            for (int i = 0; i < lives; i++)
            {
                sp.drawStatic(g, (int) (CX + i * (sp.w + 5)), (int) CY - 10);
            }
        } else if (gameState == WON)
        {
            playerHP = playerMaxHP;
            g.drawImage(youWinScreen, 0, 0, W, H, null);
        }
    }

    private void drawLayer(Graphics g, TileLayer tl, int depth)
    {
        double currScreenX = (screenX - CX * (1 - depth)) / depth; // Transform coordinates
        double currScreenY = screenY;
        if (tl.size() > 0)
        {     
            int bSX = Math.max((int) ((currScreenX - CX) / mapTileW), 0);
            int bFX = Math.min((int) ((currScreenX + CX) / mapTileW) + 1, (int) (tl.get(tl.size() - 1).x) / mapTileW);
            int sX = 0;
            int fX = 0;
            while (tl.get(sX + 1).x / mapTileW < bSX)
            {
                sX++;
            }
            while (tl.get(fX).x / mapTileW < bFX)
            {
                fX++;
            }

            for (int i = sX; i < tl.size(); i++)
            {
                tl.get(i).draw(g, currScreenX, currScreenY,tm.get(tl.get(i).gid));
            }
        }
    }

    private void drawLayer(Graphics g, TileLayer tl, int depth, double x0, double y0)
    {
        if (tl.size() > 0)
        {
            screenX = (x0 - CX * (1 - depth)) / depth;
            screenY = y0;
            int bSX = Math.min((int) ((screenX - CX) / mapTileW), 0);
            int bFX = Math.min((int) ((screenX + CX) / mapTileW) + 1, (int) (tl.get(tl.size() - 1).x) / mapTileW);
            int sX = 0;
            int fX = 0;
            while (tl.get(sX).x / mapTileW < bSX)
            {
                sX++;
            }
            while (tl.get(fX).x / mapTileW < bFX)
            {
                fX++;
            }
            while ((fX + 1 < tl.size()) && tl.get(Math.min(fX + 1, tl.size() - 1)).x / mapTileW == tl.get(fX).x / mapTileW)
            {
                fX++;
            }
            //for (int i = sX; i <= fX; i++)
            for (int i = sX; i <= fX; i++)
            {
                tl.get(i).draw(g, screenX - CX, screenY - CY);
            }
        }
    }

    public void drawPlayerStatus(Graphics g)
    {
        g.setColor(Color.RED);
        g.fillRect(x1HP, y1HP, x2HP - x1HP, y3HP - y1HP);
        g.fillPolygon(xPsHP, yPsHP, 5);
        g.fillRect(x3HP, y1HP, x4HP - x3HP, y2HP - y1HP);

        g.setColor(Color.GREEN);

        int xF = (hBarW * playerHP) / playerMaxHP + hBarX;

        if (xF > x3HP)
        {
            g.fillRect(x1HP, y1HP, x2HP - x1HP, y3HP - y1HP);
            g.fillPolygon(xPsHP, yPsHP, 5);
            g.fillRect(x3HP, y1HP, (playerHP * hBarW) / playerMaxHP + x1HP - x3HP, y2HP - y1HP);
        } else if (xF > x2HP)
        {

            int yF = y3HP - ((y3HP - y2HP) * (xF - x2HP)) / (x3HP - x2HP);
            int[] xP2 =
            {
                x2HP, x2HP, xF, xF, x2HP
            };
            int[] yP2 =
            {
                y3HP, y1HP, y1HP, yF, y3HP
            };

            g.fillRect(x1HP, y1HP, x2HP - x1HP, y3HP - y1HP);
            g.fillPolygon(xP2, yP2, 5);
        } else
        {
            g.fillRect(x1HP, y1HP, xF - hBarX + 2, y3HP - y1HP);
        }

        g.drawImage(hBarImg, x1HP-2, y1HP, hBarW+4, hBarH, null);
        g.drawImage(sHatImg,x4HP+(int)(0.25*mapTileW),y1HP,mapTileW,(mapTileW*56)/77,null);
        //sp.drawStatic(g, x4HP, y1HP);
        switch(lives)
        {
            case 0: 
                g.drawImage(x0, x4HP+3*mapTileW/2, y1HP, mapTileW, mapTileW, null);
                break;
            case 1:
                g.drawImage(x1, x4HP+3*mapTileW/2, y1HP, mapTileW, mapTileW, null);
                break;
            case 2:
                g.drawImage(x2, x4HP+3*mapTileW/2, y1HP, mapTileW, mapTileW, null);
                break;
            default:
                g.drawImage(x3, x4HP+3*mapTileW/2, y1HP, mapTileW, mapTileW, null);
                break;
        }
    }

    public void drawBossStatus(Graphics g,int bHP,int bHPMax)
    {
        g.setColor(new Color(110,18,0));
        g.fillRect(x3HPBoss, y1HPBoss, x4HPBoss - x3HPBoss, y3HPBoss - y1HPBoss);
        g.fillPolygon(xPsHPBoss, yPsHPBoss, 5);
        g.fillRect(x1HPBoss, y1HPBoss, x2HPBoss - x1HPBoss, y2HPBoss - y1HPBoss);

        g.setColor(Color.YELLOW);

        int xF = hBarW - (hBarW*bHP) / bHPMax + x1HPBoss;

        if (x2HPBoss > xF)
        {
            g.fillRect(x3HPBoss, y1HPBoss, x4HPBoss - x3HPBoss, y3HPBoss - y1HPBoss);
            g.fillPolygon(xPsHPBoss, yPsHPBoss, 5);
            g.fillRect(xF, y1HPBoss, x2HPBoss-xF, y2HPBoss - y1HPBoss);
            
        }
        else if (x3HPBoss > xF)
        {

            int yF = y2HPBoss + ((y3HPBoss - y2HPBoss) * (xF - x2HPBoss)) / (x3HPBoss - x2HPBoss);
            int[] xP2 =
            {
                xF,xF,x3HPBoss,x3HPBoss,xF
//                x2HP, x2HP, xF, xF, x2HP
            };
            int[] yP2 =
            {
                yF,y1HPBoss,y1HPBoss,y3HPBoss,yF
              //  y3HP, y1HP, y1HP, yF, y3HP
            };

            g.fillRect(x3HPBoss, y1HPBoss, x4HPBoss - x3HPBoss, y3HPBoss - y1HPBoss);
            g.fillPolygon(xP2, yP2, 5);
        } 
        else
        {
            g.fillRect(xF, y1HP, x4HPBoss-xF - 2, y3HP - y1HP);
        }

        g.drawImage(hBarBossImg, x1HPBoss-2, y1HPBoss, hBarWBoss+4, hBarHBoss, null);    
        g.drawImage(bossBustImg,x1HPBoss-3*mapTileW/2,y1HPBoss,3*mapTileW/2,3*mapTileH/2,null);
    }
    
    
    public void setSpriteState()
    {
        char tC = sp.getState();
        if (!ground)
        {
            if ((tC == 'r') || (tC == 'j') || (tC == 'g'))
            {
                if (sp.vy < 0)
                {
                    sp.setState('j');
                } else
                {
                    sp.setState('g');
                }
            } else
            {
                if (sp.vy < 0)
                {
                    sp.setState('J');
                } else
                {
                    sp.setState('G');
                }
            }
        } else if (sp.vx > 0)
        {
            sp.setState('r');
        } else if (sp.vx < 0)
        {
            sp.setState('l');
        } else if (tC == 'r' || tC == 's' || tC == 'j')
        {
            sp.setState('s');
        } else
        {
            sp.setState('S');
        }
    }
    
    private void updateCamera()
    {
    // First two cases take precedence: Never display regions outside the map
        cameraLock[0] = cameraLock[1] = cameraLock[2] = cameraLock[3] = 0;
        if(sp.x-CX + cameraTolLR < 0)
        { 
            screenX = 0;
            cameraLock[0]=1;
        }
        else if (sp.x+CX - cameraTolLR> levelMaxX)
        {
            screenX = levelMaxX - W;
            cameraLock[2] = 1;
        }
    // Then, if player outside the drawing rectangle move camera to put him in.
        else if(sp.x-screenX > CX+cameraTolLR) 
        {
            screenX = sp.x-CX-cameraTolLR;
        }
        else if(sp.x-screenX < CX-cameraTolLR) 
        {
            screenX = sp.x-CX+cameraTolLR;
        }
    // OK, now do the same for the y-direction    
        if(sp.y-CY + cameraTolU < 0)
        {
            cameraLock[1]=1;
            screenY = 0;
        }
        else if (sp.y + CY - cameraTolD > levelMaxY)
        {
            cameraLock[3] = 1;
            screenY = levelMaxY-H;
        }
        else if (sp.y-screenY>CY+cameraTolD)
        {
            screenY = sp.y-CY - cameraTolD;
        }
        else if (sp.y-screenY<CY-cameraTolU)
        {
            screenY = sp.y-CY + cameraTolU;
        }
    }
    
    private boolean isOnScreen(Squob s)
    { // This could be more efficient: 1. by using SX, and 2. by using SY
        if ((s.x > sp.x - 2 * CX) && (s.x < sp.x + 2 * CX))
        {
            return true;
        } else
        {
            return false;
        }
    }

    private void playClip(char c)
    {
        switch (c)
        {
            case 'a':
                clips.DEATH_GATOR.play();
                break;
            case 'b':
                clips.DEATH_BOSS.play();
                break;
            case 'g':
                clips.DEATH_GREAPER.play();
                break;
            case 'r':
                clips.DEATH_ROBOB.play();
                break;
            case 'B':
                clips.BOSS_HIT.play();
                break;
        }
    }

    /* ***************************************************************************
     *   ******************** Keyboard Handler ********************
     * ************************************************************************** */
    public void handleInput(char code, KeyEvent e)
    {
        switch (code)
        {
            case 'p': // Key Pressed
                if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                    up = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN)
                {
                    down = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                {
                    right = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT)
                {
                    left = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE)
                {
                    if (ground)
                    {
                        jump = true;
                    }
                }
                break;
            case 'r':  // Released
                if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                    up = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN)
                {
                    down = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                {
                    right = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT)
                {
                    left = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE)
                {
                    if (ground)
                    {
                        jump = false;
                    }
                }
                break;
            case ('t'): // Typed
                if (gameState == INTRO)
                {
                    gameState = LOADLEVEL;
                }
                if (e.getKeyChar() == 'd')
                {// For debugging press 'd' during program to spit out what lays below
                    //System.out.println("Stiles,Tiles size = (" + stiles.size() + "," + tiles.size() + ")");
                    System.out.println("(" + sp.x + "," + sp.y + "");
                    System.out.println("["+cameraLock[0]+","+cameraLock[1]+","+cameraLock[2]+","+cameraLock[3]+"]");
                    drawCamera=!drawCamera;
                    //System.out.println("Is in? "+ tiles.get(119).isIn(sp,0,0));
                    //System.out.println("");
                    //playerHP--;
                }
                break;
        }
    }

    /* ***************************************************************************
     *        ********************** Level Loader ***************************    *
     *  **************************************************************************/
    public void loadLevel(String fName)
    {
        resetTiles();
        int currTileSetWidth = 0;
        int currTileSetHeight = 0;
        int currGID = 0;
        int currLayerIndex = -3;

        int tileSetCounter = 0;
        int layerCounter = 0;

        XMLEvent event;
        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(fName);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext())
            {
                event = eventReader.nextEvent();
                if (event.isStartElement())
                {
                    StartElement startElement = event.asStartElement();
                    Iterator<Attribute> attributes = startElement.getAttributes();
                    if (startElement.getName().getLocalPart() == "map")
                    {
                        System.out.println("Map File Detected...");
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            switch (attribute.getName().toString())
                            {
                                case ("version"):
                                    break;
                                case ("orientation"):
                                    break;
                                case ("width"):
                                    mapW = Integer.parseInt(attribute.getValue());
                                    break;
                                case ("height"):
                                    mapH = Integer.parseInt(attribute.getValue());
                                    break;
                                case ("tilewidth"):
                                    mapGridSize = Integer.parseInt(attribute.getValue());
                                    mapTileW = (int)(hScale*mapGridSize);
                                    mapTileH = (int)(vScale*mapGridSize);
                                    break;
                                case ("tileheight"): // c'mon, let's only work with square tiles bud.
                                    //mapTileH = Integer.parseInt(attribute.getValue());
                                    break;
                                default:
                                    break;
                            }
                        }
                        System.out.println("Found " + mapW + "x" + mapH + " tile map with " + mapTileW + "x" + mapTileH + " pixel tiles.");
                    }
                    if (startElement.getName().getLocalPart() == "tileset")
                    {
                        System.out.println("Parsing Tileset " + tileSetCounter);
                        String tName = "";
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            switch (attribute.getName().toString())
                            {
                                case ("firstgid"):
                                    currGID = Integer.parseInt(attribute.getValue());
                                    break;
                                case ("name"):
                                    tName = attribute.getValue();
                                    System.out.println("\tTileset Name = " + tName);
                                    break;
                                case ("tilewidth"):
                                    currTileSetWidth = Integer.parseInt(attribute.getValue());
                                    break;
                                case ("tileheight"):
                                    currTileSetHeight = Integer.parseInt(attribute.getValue());
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (tName.equals("Sprites"))
                        {
                            spriteGID = currGID;
                        }
                        if (tName.substring(0, 3).equals("fg_"))
                        {
                            slopeStartIdx = currGID + slopeDelta;
                            slopeStopIdx = slopeStartIdx + numSlopes;
                        }
                    }
                    if (startElement.getName().getLocalPart() == "image")
                    {
                        System.out.println("Parsing image for tileset " + tileSetCounter);
                        String tileName = "";
                        int imageWidth = 480;
                        int imageHeight = 30;
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next(); // Bug - currently ignores first element.
                            switch (attribute.getName().toString())
                            {
                                case ("source"):
                                    tileName = levelPath + attribute.getValue();
                                    System.out.println("TileName: "+tileName);
                                    break;
                                case ("width"):
                                    imageWidth = Integer.parseInt(attribute.getValue());
                                    break;
                                case ("height"):
                                    imageHeight = Integer.parseInt(attribute.getValue());
                                    break;
                                default:
                                    break;
                            }
                        }
                        try
                        {
                            BufferedImage tIm = ImageIO.read(new File(tileName));
                            tm.add(tIm, currGID - 1, imageWidth / currTileSetWidth, currTileSetWidth, currTileSetHeight);
                            System.out.println("\tAdded tileset " + tileName + ".\n\t" + imageWidth / currTileSetWidth + " tiles of width " + currTileSetWidth);
                        } catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                        tileSetCounter++;
                    }
                    if (startElement.getName().getLocalPart() == "layer")
                    {
                        System.out.println("Parsing Layer " + layerCounter);
                        String tName = "";
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            switch (attribute.getName().toString())
                            {
                                case ("name"):
                                    tName = attribute.getValue();
                                    switch (tName)
                                    {
                                        case "tiles":
                                            currLayerIndex = 0;
                                            break;
                                        case "bg1":
                                            currLayerIndex = 1;
                                            break;
                                        case "bg2":
                                            currLayerIndex = 2;
                                            break;
                                        case "bg3":
                                            currLayerIndex = 3;
                                            break;
                                        case "fg1":
                                            currLayerIndex = 4;
                                            break;
                                        case "spikes":
                                            currLayerIndex = 5;
                                            break;
                                        case "spriteLayer":
                                            currLayerIndex = -1;
                                            break;
                                        default:
                                            currLayerIndex = -2;
                                    }
                                    System.out.println("\tName of Layer = " + tName + " with index " + currLayerIndex);
                                    break;
                                case ("width"):
                                    System.out.println("\tWidth of Layer = " + attribute.getValue() + " units");
                                    break;
                                case ("height"):
                                    System.out.println("\tHeight of Layer = " + attribute.getValue() + " units");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    if (startElement.getName().getLocalPart() == "data")
                    {
                        System.out.println("Parsing Data for Layer " + layerCounter);
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next(); // Bug - currently ignores first element.
                            switch (attribute.getName().toString())
                            {
                                case ("encoding"):
                                    System.out.println("\tEncoding = " + attribute.getValue());
                                    break;
                                default:
                                    break;
                            }
                        }
                        event = eventReader.nextEvent();
                        String stEvent = "";
                        while (!event.isEndElement())
                        {
                            stEvent = stEvent + event.toString().replace("\n", "").replace("\r", ""); // Tiled places extra newlines in csv portion
                            event = eventReader.nextEvent();
                        }
                        if (currLayerIndex == -1)
                        {
                            loadSprites(stEvent, mapW, mapH, mapTileW, mapTileH);
                        } else
                        {
                            System.out.println("Loading Layer with index " + currLayerIndex);
                            loadLayer(currLayerIndex, stEvent, mapW, mapH, mapTileW, mapTileH);
                        }
                        layerCounter++;
                    }
                }
            }
            stiles.sort();           
            tiles.sort();
            bg3.sort();
            bg2.sort();
            bg1.sort();
            fg1.sort();
            spikes.sort();
            removeInternalFaces();
            
            sp.resize(mapTileW, 3*mapTileW/2);
            
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (XMLStreamException e)
        {
            e.printStackTrace();
        }
// End of XML parsing        
    }

    public boolean loadLayer(int index, String data, int nX, int nY, int tW, int tH)
    {
        String[] tokens = data.split(",");
        if (tokens.length != nX * nY)
        {
            System.out.println("Error decoding file");
            return false;
        } else
        {
            int cnt = 0;
            int tmp = 0;
            for (int j = 0; j < nY; j++)
            {
                for (int i = 0; i < nX; i++)
                {
                    tmp = Integer.parseInt(tokens[cnt]);
                    if (tmp != 0) // 0 is taken to represent an empty slot
                    {
                        TileImagePoint ttp = tm.getTileImage(tmp - 1);
                        int tHash = tmp-1;//iMan.add(createImage(ttp.getImage().getSource()));
                        switch (index)
                        {
                            case 0:
                                if (!isStile(tmp))
                                {
                                    tiles.add(new Tile(i * mapTileW, j * mapTileH, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, tmp - 1, tHash));
                                } else
                                {
                                    //stiles.add(new Stile(i * mapTileW, j * mapTileH, ttp.width, ttp.height, getSlopeCoord('l', tmp - slopeStartIdx), getSlopeCoord('r', tmp - slopeStartIdx), tmp - 1, tIm));
                                    stiles.add(new Stile(i * mapTileW, j * mapTileH, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, getSlopeCoord('l', tmp - slopeStartIdx), getSlopeCoord('r', tmp - slopeStartIdx), tmp - 1, tHash));
                                }
                                break;
                            case 1:
                                bg1.add(new Tile(i * mapTileW, (j+1) * mapTileH - ttp.height, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, tmp - 1, tHash));
                                break;
                            case 2:
                                bg2.add(new Tile(i * mapTileW, (j+1) * mapTileH - ttp.height, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, tmp - 1, tHash));
                                break;
                            case 3:
                                bg3.add(new Tile(i * mapTileW, (j+1) * mapTileH - ttp.height, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, tmp - 1, tHash));
                                break;
                            case 4:
                                fg1.add(new Tile(i * mapTileW, (j+1) * mapTileH - ttp.height, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, tmp - 1, tHash));
                                break;
                            case 5:
                                spikes.add(new Tile(i * mapTileW, (j+1) * mapTileH - ttp.height, (mapTileW*ttp.width)/mapGridSize, (mapTileH*ttp.height)/mapGridSize, tmp - 1, tHash));
                                break;
                            default:
                                //tiles.add(tTile);
                                break;
                        }
                    }
                    cnt++;
                }
            }
            return true;
        }
    }

    public boolean loadSprites(String data, int nX, int nY, int tW, int tH)
    {
        // start sprite GID
        System.out.println("Sprite GID = " + spriteGID);
        String[] tokens = data.split(",");
        if (tokens.length != nX * nY)
        {
            System.out.println("Error decoding file");
            return false;
        } else
        {
            int cnt = 0;
            int cntE = 0;
            numEnemies = 0;
            int tmp = 0;
            for (int k = 0; k < tokens.length; k++)
            {
                if (Integer.parseInt(tokens[cnt]) - spriteGID > 0)
                {
                    numEnemies++;
                }
                cnt++;
            }
            cnt = 0;
System.out.println("There are "+numEnemies+" enemies.");
            enemies = new Enemy[numEnemies];
            for (int j = 0; j < nY; j++)
            {
                for (int i = 0; i < nX; i++)
                {
                    tmp = Integer.parseInt(tokens[cnt]) - spriteGID;
                    if(tmp>=0)
                    {
                        System.out.println("Adding Sprite:" + tmp);
                        switch (tmp)
                        {
                            case 0:
                                sp.x = playerStartX = sp.x = i * mapTileW;
                                sp.y = playerStartY = sp.y = j * mapTileH;
                                break;
                            case 1:
                                enemies[cntE] = new Greaper(i * mapTileW, j * mapTileH, mapTileW, mapTileH, spritePath, grav);
                                cntE++;
                                break;
                            case 2:
                                enemies[cntE] = new Robob(i * mapTileW, j * mapTileH, (4*mapTileW)/3, mapTileH, spritePath, grav);
                                cntE++;
                                break;
                            case 3:
                                enemies[cntE] = new Gator(i * mapTileW, j * mapTileH, mapTileW, mapTileH, spritePath, grav);
                                cntE++;
                                break;
                            case 4:
                                bossIndex = cntE;
                                enemies[cntE] = new Boss(i * mapTileW, j * mapTileH, 4 * mapTileW, 2 * mapTileH, spritePath,grav);
                                cntE++;
                                break;
                            case 5:
                                enemies[cntE] = new Cannon(i * mapTileW, j * mapTileH, mapTileW, mapTileH, spritePath, grav);
                                enemies[cntE].setState('l');
                                cntE++;
                                break;
                            case 6:
                                enemies[cntE] = new Cannon(i * mapTileW, j * mapTileH, mapTileW, mapTileH, spritePath, grav);
                                enemies[cntE].setState('r');
                                cntE++;
                                break;
                            default:
                                System.out.println("Error: atttempted to add unknown enemy type");
                                break;
                        }                       
                    }
                    cnt++;
                }
            }
            return true;
        }

    }

    private void resetTiles()
    {
        tiles = new TileLayer(10000);
        stiles = new TileLayer(10000);
        fg1 = new TileLayer(10000);
        bg1 = new TileLayer(10000);
        bg2 = new TileLayer(10000);
        bg3 = new TileLayer(10000);
        spikes = new TileLayer(10000);
        tm = new TileImageManager(250);
    }

// during levelloading, most tiles have square bounding collision boxes, except sloped tiles.
    private boolean isStile(int i)
    {
        if (slopeStartIdx < i && i <= slopeStopIdx)
        {
            return true;
        } else
        {
            return false;
        }
    }

    private double getSlopeCoord(char side, int idx)
    {
        double ret = -100;
        switch (idx)
        {
            case (1):
                if (side == 'l')
                {
                    ret = mapTileH;
                } else
                {
                    ret = mapTileH / 2;
                }
                break;
            case (2):
                if (side == 'l')
                {
                    ret = mapTileH / 2;
                } else
                {
                    ret = 0;
                }
                break;
            case (3):
                if (side == 'l')
                {
                    ret = 0;
                } else
                {
                    ret = mapTileH / 2;
                }
                break;
            case (4):
                if (side == 'l')
                {
                    ret = mapTileH / 2;
                } else
                {
                    ret = mapTileH;
                }
                break;
            case (5):
                if (side == 'l')
                {
                    ret = mapTileH;
                } else
                {
                    ret = 0;
                }
                break;
            case (6):
                if (side == 'l')
                {
                    ret = 0;
                } else
                {
                    ret = mapTileH;
                }
                break;
        }
        return ret;
    }

    private void loadPlayer()
    {// Basically, I wanted to move this unsightly code elsewhere at the time ..
        String[] fNames_r =
        {
            spritePath + "smiley_r_1.PNG", spritePath + "smiley_r_2.PNG",
            spritePath + "smiley_r_3.PNG", spritePath + "smiley_r_4.PNG",
            spritePath + "smiley_r_5.PNG", spritePath + "smiley_r_6.PNG",
            spritePath + "smiley_r_7.PNG", spritePath + "smiley_r_8.PNG",
            spritePath + "smiley_r_9.PNG", spritePath + "smiley_r_10.PNG",
            spritePath + "smiley_r_11.PNG"
        };
        String[] fNames_l =
        {
            spritePath + "smiley_l_1.PNG", spritePath + "smiley_l_2.PNG",
            spritePath + "smiley_l_3.PNG", spritePath + "smiley_l_4.PNG",
            spritePath + "smiley_l_5.PNG", spritePath + "smiley_l_6.PNG",
            spritePath + "smiley_l_7.PNG", spritePath + "smiley_l_8.PNG",
            spritePath + "smiley_l_9.PNG", spritePath + "smiley_l_10.PNG",
            spritePath + "smiley_l_11.PNG"
        };
        String[] fNames_ju =
        {
            spritePath + "smiley_j_r_u.PNG"
        };
        String[] fNames_Ju =
        {
            spritePath + "smiley_j_l_u.PNG"
        };
        String[] fNames_jd =
        {
            spritePath + "smiley_j_r_d.PNG"
        };
        String[] fNames_Jd =
        {
            spritePath + "smiley_j_l_d.PNG"
        };
        String[] fNames_s =
        {
            spritePath + "smiley_stand_r_1.PNG"
        };
        String[] fNames_S =
        {
            spritePath + "smiley_stand_l_1.PNG"
        };

        sp = new Player(0, 0, pw, ph, fNames_r, fNames_l, fNames_ju, fNames_Ju, fNames_jd, fNames_Jd, fNames_s, fNames_S, 16);
        System.out.println("Created new player at ("+sp.x+","+sp.y+")");
    }

    /* ***************************************************************************
     *    ********************** Collision handling **************************
     *  **************************************************************************/
    private void handlePlayerCollisions()
    { // For now, handle sloped tiles differently than tiles       
        int[] tmpBnds = getBounds(sp, 2, tiles);
        int uL = tmpBnds[0];
        int uR = tmpBnds[1];

        boolean[] sides =
        {
            false, false, false, false
        };
        for (int i = uL; i <= uR; i++)
        {
            if (tiles.get(i).isIn(sp, sp.vx, sp.vy)) // Only if there is a collision ...
            {
                sides = tiles.get(i).getSide(sp, sp.vx, 0);
                if (sides[0] || sides[2])
                {
                    sp.vx = 0;
                }
                sides = tiles.get(i).getSide(sp, 0, sp.vy);
                if (sides[1] || sides[3])
                {
                    sp.vy = 0;
                }
                if (sides[1])
                {
                    ground = true;
                }
            }
        }

        tmpBnds = getBounds(sp, 2, stiles);
        uL = tmpBnds[0];
        uR = tmpBnds[1];

        for (int i = uL; i <= uR; i++)
        {
            if (stiles.get(i).isInFace(sp, sp.vx, sp.vy)) // Only if there is a collision ...
            {
                sp.vy = 0;
                ground = true;
                sp.y = stiles.get(i).lockY(sp);
            }
        }
    }

    private void handleNPCCollisions()
    {
        boolean tWCH = false, tWCV = false, tWF = false, tWCS = false;
        int uL, uR;
        int[] tmpBnds;
        boolean[] sides =
        {
            false, false, false, false
        };
// OK, now that that's done, handle collisions. For each enemy:
        for (int i = 0; i < numEnemies; i++)
        {
            if (isOnScreen(enemies[i]))
            {
                if (enemies[i].isAlive())
                {
                    tmpBnds = getBounds(enemies[i], (int) (enemies[i].w / mapTileW) + 1, tiles);
                    uL = tmpBnds[0];
                    uR = tmpBnds[1];

                    for (int j = uL; j < uR; j++)
                    {
                        if (tiles.get(j).isIn(enemies[i], enemies[i].vx, enemies[i].vy)) // Only if there is a collision ...
                        {
                            sides = tiles.get(j).getSide(enemies[i], enemies[i].vx, 0);
                            if (sides[0] || sides[2])
                            {
                                tWCH = true;
                            }
                            sides = tiles.get(j).getSide(enemies[i], 0, enemies[i].vy);
                            if (sides[1] || sides[3])
                            {
                                tWCV = true;
                            }
                        }
                    }

                    tmpBnds = getBounds(enemies[i], (int) (enemies[i].w / mapTileW) + 1, stiles);
                    uL = tmpBnds[0];
                    uR = tmpBnds[1];

                    for (int j = uL; j < uR; j++)
                    {
                        if (stiles.get(j).isInFace(enemies[i], enemies[i].vx, enemies[i].vy)) // Only if there is a collision ...
                        {
                            tWCS = true;
                            enemies[i].setSlope(stiles.get(j));
                        }
                    }
                    for (int j = 0; j < enemies.length; j++)
                    {
                        if ((i != j) && enemies[i].isIn(enemies[j], 0, 0) && enemies[j].isAlive())
                        {
                            tWCH = true;
                        }
                    }
                }
                enemies[i].iterate(0.5, tWCH, tWCV, false, tWCS, 0, grav);
                evacuate(enemies[i]);
                tWCH = tWCV = tWCS = tWF = false;
            }
        }
    }

    private boolean evacuate(Squob s)
    { // If, through hook or crook, the sprite ends up in tile, move'im out. 
        boolean ret = false;
        int[] tmpBnds = getBounds(s, (int) (s.w / mapTileW) + 1, stiles);
        int uL = tmpBnds[0];
        int uR = tmpBnds[1];
        boolean[] sides =
        {
            false, false, false, false
        };
        for (int i = uL; i < uR; i++)
        {

            if (stiles.get(i).isInFace(s, 0, 0))
            {
                s.y = stiles.get(i).lockY(s);
                return true;
            }
        }
        tmpBnds = getBounds(s, (int) (s.w / mapTileW) + 1, tiles);
        uL = tmpBnds[0];
        uR = tmpBnds[1];
        for (int i = uL; i < uR; i++)
        {
            if (tiles.get(i).isIn(s, 0, 0)) // Only if there is a collision ...
            {

                sides = tiles.get(i).getSide(s, 0, 0);
                if (sides[0])
                {
                    s.x -= 2;
                    ret = true;
                } else if (sides[2])
                {
                    ret = true;
                    s.x += 2;
                } else if (sides[1])
                {
                    ret = true;
                    s.y -= 2;
                } else if (sides[3])
                {
                    ret = true;
                    s.y += 2;
                }
            }
        }

        return ret;
    }

    private void removeInternalFaces()
    {
        Tile s;
        System.out.print("Attempting to Remove internal faces ...\n\tStarting with Tiles...");
        int edgeCount = 0;
        int squobCount = 0;
        boolean tmpCnt = false;
        for (int i = 0; i < tiles.size(); i++)
        {
            s = tiles.get(i);
            if (tiles.isTileAt(s.x - s.w / 2, s.y + s.h / 2) || (stiles.isTileAt(s.x - s.w / 2, s.y + s.h / 2)))
            {// left
                tiles.get(i).setColliding(0, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tiles.isTileAt(s.x + s.w / 2, s.y - s.h / 2) || stiles.isTileAt(s.x + s.w / 2, s.y - s.h / 2))
            {// top
                tiles.get(i).setColliding(1, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tiles.isTileAt(s.x + 3 * s.w / 2, s.y + s.h / 2) || stiles.isTileAt(s.x + 3 * s.w / 2, s.y + s.h / 2))
            {// right
                tiles.get(i).setColliding(2, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tiles.isTileAt(s.x + s.w / 2, s.y + 3 * s.h / 2) || stiles.isTileAt(s.x + s.w / 2, s.y + 3 * s.h / 2))
            {//bottom
                tiles.get(i).setColliding(3, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tmpCnt)
            {
                squobCount++;
            }
            tmpCnt = false;
        }
        System.out.print(" Removed " + edgeCount + " faces on " + squobCount + " tiles.\n\tContinuing with Slobs...");
        Tile sl;
        edgeCount = squobCount = 0;
        tmpCnt = false;
        for (int i = 0; i < stiles.size(); i++)
        {
            sl = stiles.get(i);
            if (tiles.isTileAt(sl.x - sl.w / 2, sl.y + sl.h / 2) || (stiles.isTileAt(sl.x - sl.w / 2, sl.y + sl.h / 2)))
            {// left
                stiles.get(i).setColliding(0, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (stiles.isTileAt(sl.x + sl.w / 2, sl.y - sl.h / 2) || stiles.isTileAt(sl.x + sl.w / 2, sl.y - sl.h / 2))
            {// top
                stiles.get(i).setColliding(1, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tiles.isTileAt(sl.x + 3 * sl.w / 2, sl.y + sl.h / 2) || stiles.isTileAt(sl.x + 3 * sl.w / 2, sl.y + sl.h / 2))
            {// right
                stiles.get(i).setColliding(2, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tiles.isTileAt(sl.x + sl.w / 2, sl.y + 3 * sl.h / 2) || stiles.isTileAt(sl.x + sl.w / 2, sl.y + 3 * sl.h / 2))
            {//bottom
                stiles.get(i).setColliding(3, false);
                edgeCount++;
                tmpCnt = true;
            }
            if (tmpCnt)
            {
                squobCount++;
            }
            tmpCnt = false;
        }
        System.out.print(" Removed " + edgeCount + " faces on " + squobCount + " tiles.\nDone Removing internal faces.");
    }

    public int[] getBounds(Squob s, int N, TileLayer t)
    { // Do a binary search to find indices of tiles in TileLayer t within a (square) radius of N tiles from tile at (x0,y0)
        int rX, lX;

        int L = Math.max((int) (s.x / mapTileW - N), 0); // grid-cordinate of leftmost tileto check
        int R = Math.min((int) (s.x / mapTileW + N), (int) t.get(t.size() - 1).x / mapTileW);

        int xI = 0;
        int xF = t.size(); // index of leftmost and rightmost tile.
        lX = (xI + xF) / 2;
        int cur;

        boolean found = false;

// find left bound        
        while (!found && (xF - xI) > 1)
        {
            cur = (int) t.get(lX).x / mapTileW;
            if (cur == L)
            {
                found = true;
            } else if (cur > L)
            {
                xF = lX;
            } else
            {
                xI = lX;
            }
            lX = (xI + xF) / 2;
        }

        if ((int) t.get(lX).x / mapTileW == L)
        { //  the x-coordinates needn't be unique, we want the first one
            while ((int) t.get(Math.max(lX - 1, 0)).x / mapTileW == L && lX > 0)
            {
                lX--;
            }
        } else
        { // If the value was not found, we want the next lowest point
            if ((int) t.get(lX).x / mapTileW > L)
            {
                lX = Math.max(0, lX - 1);
            }
        }

// ... now the right bound 
        xI = 0;
        xF = t.size();
        rX = (xI + xF) / 2;
        found = false;
        while (!found && (xF - xI) > 1)
        {
            cur = (int) t.get(rX).x / mapTileW;
            if (cur == R)
            {
                found = true;
            } else if (cur > R)
            {
                xF = rX;
            } else
            {
                xI = rX;
            }
            rX = (xI + xF) / 2;
        }
// we the x-coordinates needn't be unique, we want the first one
        if ((int) t.get(rX).x / mapTileW == R)
        { //  the x-coordinates needn't be unique, we want the first one
            while ((int) t.get(Math.min(rX + 1, t.size())).x / mapTileW == R && rX < t.size())
            {
                rX++;
            }
        } else
        { // If the value was not found, we want the next lowest point
            if ((int) t.get(rX).x / mapTileW < R)
            {
                rX = Math.min(t.size(), rX + 1);
            }
        }
        int[] ret =
        {
            lX, rX
        };
        return ret;
    }
    
    private void tic()
    {
        gClock = System.currentTimeMillis();
        if(gClockEnabled)
        {
            System.out.println("Warning: game clock re-loaded without toc()");
        }
        gClockEnabled = true;
    }
    private void toc()
    {
        if(gClockEnabled)
        {
            System.out.println("Time elapsed: "+(System.currentTimeMillis()-gClock)+".\t gameState: "+gameState);
            gClockEnabled = false;
        }
        else
        {
            System.out.println("Error: game Clock not loaded. Use tic() before toc().");
        }
    }
}