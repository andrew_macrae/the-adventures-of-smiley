package smiley;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class Enemy extends Squob
{
    protected String idStr;
    protected boolean alive;
    protected char state, type;
    public double vx, vy;
    protected boolean ground;
    protected int hitPoints,hpCounter,maxHitPoints,hpCnt;
    protected boolean jumper;
    protected Tile slope;
    protected Image hBarImg;
    protected double playerVicinity;
    protected double playerX,playerY;
    protected double playerVelX,playerVelY;    
    protected BuList bList;
    protected int fireCounter;
    protected int fireDeadTime;
    protected int hpW,hpH,hpDX,hpDY;
    protected double grav;
// Type: (c)hompy, (g)reaper, (r)obob, g(a)tor, (B)oss    

    public Enemy(double xs, double ys, double ws, double hs,double g)
    {
        super(xs, ys, ws, hs);
        grav = g;
        
        alive = true;
        state = 'r';
        vx = 0.5;
        vy = 0;
        ground = false;
        hitPoints = maxHitPoints = 1;
        jumper = false;
        slope = new Stile(0,0,0,0,0,0,0,-1);
        
        hpDX = 10;
        hpDY = 14;
        hpW = 30;
        hpH = 10;
        
        playerVicinity = 1E3; // Scienterrific notation!!!
        
        bList = new BuList(1);
        
        idStr = "enemy";
        
        try
        {
            hBarImg  = ImageIO.read(new File("Images/Screens/hpBar_enemy.png"));
        } catch (IOException ex)
        {
            Logger.getLogger(Enemy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isAlive()
    {
        return alive;
    }

    public void setAlive(boolean b)
    {
        alive = b;
    }

    public boolean addImage(Image i)
    {
        return false;
    }

    public boolean setState(char c)
    {
        state = c;
        return true;
    }

    public char getState()
    {
        return state;
    }
// To be called by child Class

    public void iterate(double d, boolean collH, boolean collV, boolean willFall, boolean onSlope, double aX, double aY)
    {
        if (isAlive())
        {
            ground = false;
        // apply accelerations
            vx+=aX;
            vy+=aY;
        // If on a slope, lock y to the slope surface
            if (onSlope)
            {
                vy=0;
                y = slope.lockY(this);
                ground = true;
            }

            if (!jumper)
            {
                if (collH ^ willFall) // ^ is Java's XOR operator. Had to try it.
                {
                    vx = -vx;
                }

                if (collV)
                {
                    if ((vy > 0) && !willFall)
                    {
                        ground = true;
                        vy = 0;
                    }
                }
            } else
            {
                if (collH) // ^ is Java's XOR operator. Had to try it.
                {
                    vx = -vx;
                }

                if (collV)
                {
                    if (vy > 0)
                    {
                        ground = true;
                        vy = 0;
                    }
                }
            }

            x += vx;
            y += vy;

        }
    }

    public void kill()
    {
        hitPoints--;
        if (hitPoints < 0)
        {
            alive = false;
            state = 'd';
        }
    }

    public int getHP()
    {
        return hitPoints;
    }
    public int getMaxHP()
    {
        return maxHitPoints;
    }    

    public char getType()
    {
        return type;
    }

    public void setSlope(Tile st)
    {
        slope = st;
    }
    protected void drawHP(Graphics g,int x0, int y0)
    {
        if(hpCounter>0)
        {
            hpCounter--;
            g.setColor(Color.RED);
            g.fillRect((int)x-x0-hpDX+2,(int)(y-y0-hpDY),hpW-3, hpH);
            g.setColor(Color.GREEN);
            g.fillRect((int)x-x0-hpDX+2,(int)(y-y0-hpDY),(hpW*hitPoints+(hpW*hpCounter)/hpCnt)/maxHitPoints-3, hpH);
            g.drawImage(hBarImg,(int)x-x0-hpDX,(int)(y-y0-hpDY),hpW, hpH,null);
        }
        
    }
    
    public void setPlayerCoords(double xP, double yP, double vX, double vY)
    {
        playerX = xP;
        playerY = yP;
        playerVelX = vX;
        playerVelY = vY;
        playerVicinity = Math.sqrt((xP-x)*(xP-x)+(yP-y)*(yP-y));
    }
    
    public boolean isInWeapon(Squob s, double dx, double dy)
    {
        //boolean ret = super.isIn(s,dx,dy);
        return bList.checkIsIn(s, dx, dy);
    }
    
    
    public String getId()
    {
        return idStr;
    }
}
