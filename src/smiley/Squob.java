package smiley;

import java.awt.*;
import java.awt.geom.Line2D;


/* ***************************************************************************
 * Squob.java: Andrew MacRae (liboff@gmail.com), 2013
 *
 * Squob, short for SQUare OBject is the base data type for the game. Almost everything on screen inherits squob.
 * All squobs have a bounding box defined by w(idth) and (h)eight which is used for drawing, as well as bounding
 * box for collisions defined relative to the box. For example along x, the box is constrained by [x+dCX,x+dCX+dCW],
 * whereas the drawing is constrained by [x,x+w]. In general a squob's face's needn't be parallel, but there must be 4.
 * A triangle is made with two degenerate corners.  Squobs are defined from the x-y coordinates bounding box with sides 
 * enumerated as [Left, Up, Right, Down]. Faces can be collidable or not by setting the boolean array collMap [L,U,R,D]. and flagged
 * via the int array sides [fL, fU, fR, fD]

 * ************************************************************************ */

public class Squob
{

    public double x, y, w, h; // Bounding box for drawing
    public double dCx, dCy, dCw, dCh; // Bounding box for collisions
//    public Image im;
    public char type;
    public int imgHash;
    public boolean drawColl = false;
    protected boolean drawDebug;
    
    public int[] sides =
    {
        0, 0, 0, 0
    };
    
    public boolean[] collMap = // the 
    {
        true, true, true, true
    };
    
    public boolean drawPhysics = false;
    
    public Squob(double xs, double ys, double ws, double hs)
    {
        x = xs;
        y = ys;
        w = ws;
        h = hs;
        dCx = 0;
        dCy = 0;
        dCw = w;
        dCh = h;
        type = 't';
        imgHash =  -1;
        drawDebug = false;
    }

    public Squob(int xs, int ys, int ws, int hs, char tp)
    {
        x = xs;
        y = ys;
        w = ws;
        h = hs;
        dCx = 0;
        dCy = 0;
        dCw = w;
        dCh = h;
        type = tp;
        drawDebug = false;
    }

    public Squob(double xs, double ys, double ws, double hs, int imgur)
    {
        x = xs;
        y = ys;
        w = ws;
        h = hs;
        dCx = 0;
        dCy = 0;
        dCw = w;
        dCh = h;
        type = 't';
        imgHash = imgur;
        drawDebug = false;
    }

    public void draw(Graphics g, double x0, double y0)
    {
        g.setColor(Color.black);
        g.fillRect((int) (x - x0), (int) (y - y0), (int) w, (int) h);
        if (drawPhysics)
        {
            if (drawColl)
            {
                g.setColor(Color.yellow);
                g.drawRect((int) (x - x0) + 1, (int) (y - y0) + 1, (int) w - 2, (int) h - 2);
            }
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.green);
            g2.setStroke(new BasicStroke(2));
            g2.setColor(Color.green);

            if (collMap[0])
            {
                g2.draw(new Line2D.Double(x + 1, y + h - 2, x + 1, y + 1));
            }
            if (collMap[1])
            {
                g2.draw(new Line2D.Double(x + 1, y + 1, x + w - 2, y + 1));
            }
            if (collMap[2])
            {
                g2.draw(new Line2D.Double(x + w - 2, y + h - 2, x + w - 2, y + 1));
            }
            if (collMap[3])
            {
                g2.draw(new Line2D.Double(x + 1, y + h - 2, x + w - 2, y + h - 2));
            }
        }
        drawColl = false;
        sides[0] = sides[1] = sides[2] = sides[3] = 0;
   }

    public void copy(Squob s)
    {
        x = s.x;
        y = s.y;
        w = s.w;
        h = s.h;
        dCx = s.dCx;
        dCy = s.dCy;
        dCw = s.dCw;
        dCh = s.dCh;
        type = s.type;
        imgHash = s.imgHash;
    }

    public boolean isIn(Squob s, double dx, double dy)
    {
        if ((s.x + s.dCx + s.dCw + dx > x + dCx)
                && (s.x + s.dCx + dx < x + dCx + dCw)
                && (s.y + s.dCy + s.dCh + dy > y + dCy)
                && (s.y + s.dCy + dy < y + dCy + dCh))
        {
            return true;
        } else
        {
            return false;
        }
    }

    public V2 getNormal(Squob s, double dx, double dy)
    { // Given a squob which is colliding with this squob, reutn normal vector to surfaces involved in the collision
        double tx = 0, ty = 0;
        double yT = s.y + s.dCy;
        double yB = yT + s.dCh;
        double xL = s.x + s.dCx;
        double xR = xL + s.dCw;
        if (yT < y && y < yB)
        {
            tx = 0;
            ty = -1;
            sides[1] = 1;
        } else if (yT < y + h && y + h < yB)
        {
            tx = 0;
            ty = 1;
            sides[3] = 1;
        } else if (xL < x + w && x + w < xR)
        {
            tx = 1;
            ty = 0;
            sides[2] = 1;
        } else if (xL < x && x < xR)
        {
            tx = -1;
            ty = 0;
            sides[0] = 1;
        }
        return new V2(tx, ty);
    }

    public void loadDrawColl()
    {
        drawColl = true;
    }

    public boolean[] getSide(Squob s, double dx, double dy)
    { //[l,u,r,d]
        boolean[] ret =
        {
            false, false, false, false
        };
        double sXL = s.x + s.dCx + dx;
        double sXR = s.x + s.dCx + s.dCw + dx;
        double sYU = s.y+s.dCy+dy;
        double sYD = s.y+s.dCy+s.dCh+dy;

        if (sXR > x && sXL < x && sYD > y && sYU < y + h)
        {//left
            ret[0] = collMap[0];
        }
        if (sXL < x + w && sXR > x + w && sYD > y && sYU < y + h)
        {//right
            ret[2] = collMap[2];
        }
        if (sYU < y && sYD > y && sXR > x && sXL < x + w)
        {//up
            ret[1] = collMap[1];
        }
        if (sYU < y + h && sYD > y + h && sXR > x && sXL < x + w)
        {//down
            ret[3] = collMap[3];
        }
        return ret;
    }

    
// AXE ??? ... 
    public boolean setColliding(int edge, boolean isColl)
    {
        if (edge < 0 || edge > 3)
        {
            return false;
        } else
        {
            collMap[edge] = isColl;
            return true;
        }
    }
    public boolean isCollLineSegment(LineSeg lA, LineSeg lB)
    { // ignoring identical/vertical lines, this is a one-liner, to make it robust you need to add a few if statements
        boolean tColl = false;
        double delta = 0.5;
        if (lA.isVert() && lB.isVert())
        {
            if (Math.abs(lA.x1 - lB.x1) < delta && lA.y2 > lB.y1 && lA.y1 < lB.y2)
            {
                tColl = true;
            }
        } else if (lA.isVert())// vertical line 1
        {
            double m2 = lB.height() / lB.width();
            double b2 = lB.y1 - m2 * lB.x1;
            double yI = m2 * lA.x1 + b2;
            if (yI <= lA.y2 && yI >= lA.y1 && lA.x1 >= lB.x1 && lA.x2 <= lB.x2)
            {
                tColl = true;
            }
        } else if (lB.isVert())// vertical line 2
        {
            double m1 = lA.height() / lA.width();
            double b1 = lA.y1 - m1 * lA.x1;
            double yI = m1 * lB.x1 + b1;
            if (yI < lB.y2 && yI > lB.y1 && lB.x1 >= lA.x1 && lB.x2 <= lA.x2)
            {
                tColl = true;
            }
        } else
        {
            double m1 = lA.height() / lA.width();
            double m2 = lB.height() / lB.width();
            double b1 = lA.y1 - m1 * lA.x1;
            double b2 = lB.y1 - m2 * lB.x1;
            if ((Math.abs(m1 - m2) < delta) && (Math.abs(b1 - b2) < delta)) // if they have the same slope, they must have the same intercept
            { // ... then there's a collision if they lay along a common line
                if (lA.x1 < lB.x2 && lA.x2 > lB.x1)
                {
                    tColl = true;
                }
            } else // This is what the code looks like without special cases...
            {
                double xI = (b2 - b1) / (m1 - m2);
                if ((xI >= lA.x1 && xI <= lA.x2) && (xI >= lB.x1 && xI <= lB.x2))
                {
                    tColl = true;
                }
            }
        }
        return tColl;
    }
    
    protected int sign(double d)
    {
        if(d<0) return -1;
        else return 1;
    }
}
