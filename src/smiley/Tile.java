package smiley;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Line2D;

/**
 *
 * @author Andrew MacRae <macrae@berkeley.edu>
 */
public class Tile extends Squob
{
    boolean oneTimeDebug = false;
    int gid = -1;

    public Tile(double xs, double ys, double ws, double hs, int g, int img)
    {
        super(xs, ys, ws, hs, img);
        gid = g;
        imgHash = img;
    }

    public void draw(Graphics g, double x0, double y0, Image im)
    {
        g.drawImage(im, (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
        if (drawDebug)
        {   
            g.setColor(Color.yellow);
            g.drawRect((int)(x-x0), (int)(y-y0), (int)w, (int)h);
            g.setColor(Color.green);
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(2));
            if (collMap[0])
            {
                g2.draw(new Line2D.Double(x - x0, y + h - y0, x - x0, y - y0));
            }
            if (collMap[1])
            {
                g2.draw(new Line2D.Double(x - x0, y - y0, x + w - x0, y - y0));
            }
            if (collMap[2])
            {
                g2.draw(new Line2D.Double(x - x0 + w, y + h - y0, x + w - x0, y - y0));
            }
            if (collMap[3])
            {
                g2.draw(new Line2D.Double(x - x0, y + h - y0, x + w - x0, y + h - y0));
            }
            drawDebug = false;
        }
    }

    public int getImageHash()
    {
        return imgHash;
    }

    public String toString()
    {
        return "Tile (x,y,w,h) = (" + x + "," + y + "," + w + "," + h + ")";
    }

    public V2 getSurfaceNorm(int idx)
    {
        switch (idx)
        {
            case 0:
                return new V2(-1, 0);
            case 1:
                return new V2(0, -1);
            case 2:
                return new V2(1, 0);
            case 3:
                return new V2(0, 1);
            default:
                return new V2(0, 0);
        }
    }
    public LineSeg getSurfaceLineSeg()
    {
        return new LineSeg(x, y, x+w, y);
    }    
    
    public boolean isInFace(Squob s,double dx, double dy)
    { // We really only wants this for Stiles, precious.
        LineSeg l1 = new LineSeg(x,y,x+w,y);
        LineSeg l2 = new LineSeg(s.x+s.dCx+s.dCw/2+dx,s.y+s.dCy+dy,s.x+s.dCx+s.dCw/2+dx,s.y+s.dCy+s.dCh+dy);
        return isCollLineSegment(l1,l2);
    }
    
    public double lockY(Squob s)
    { // dx = Sprite center - x1. m = slope, y(dx) = y1 + m*dx
        return y;
    }
    public double getCos()
    {
        return 1;
    }
    public double getSin()
    {
        return 0;
    }
}
