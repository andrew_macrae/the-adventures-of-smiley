/*
 *  This software is free.
 */

package smiley;

public class TileLayer
{

    private Tile[] list;
    private int size;
    private int maxSize;
    private boolean quick;

    public TileLayer(int max)
    {
        size = 0;
        maxSize = max;
        list = new Tile[maxSize];
        for (int i = 0; i < maxSize; i++)
        {
            list[i] = new Tile(0, 0, 0, 0, 't',-1);
        }
        quick = true;
    }

    public int size()
    {
        return size;
    }

    public void add(Tile s)
    {
        list[size] = s;
        size++;
    }
    public void add(Stile s)
    {
        list[size] = s;
        size++;
    }
    public Tile get(int i)
    {
        if (i < size)
        {
            return list[i];
        } else
        {
            return new Tile(-1,-1,-1,-1,-1,-1);
        }
    }
    
    public boolean sort() // Currently bubbleSort since the lists are small. Could do quickSort too.
    {
        if(!quick) bubbleSort();
        else
        {
            Tile[] tmp = new Tile[size];
            for(int i = 0;i<tmp.length;i++)
            {
                tmp[i] = list[i];
            }
            tmp = quickSort(tmp);
            for(int i = 0;i<tmp.length;i++)
            {
                list[i] = tmp[i];
            }
        }
        return verifySort();
    }
    public boolean isTileAt(double x0, double y0)
    {// Get Squob at coordinates (x0,y0)
        Tile s;
        for(int i = 0;i<size;i++)
        {
            s = get(i);
            if(s.x<x0 && x0<s.x+s.w && s.y < y0 && y0 < s.y+s.h)
            {
                return true;
            }
        }
        return false;
    }
    public void bubbleSort()
    {
        Tile t;
        int lMax = size;
        while(lMax>1)
        {
            for(int i = 0;i<lMax-1;i++)
            {
                if(compare(i,i+1)>0)
                {
                    t = list[i];
                    list[i] = list[i+1];
                    list[i+1] = t;
                }
            }
            lMax--;
        }        
    }
    
    public Tile[] quickSort(Tile[] lst)
    {
        int sz = lst.length;
        if (sz < 2)
        {
            return lst;
        } else
        {
            // choose pivot
            Tile pivot = lst[0];
            int lSize = 0;
            int rSize = 0;
            //build partition lists (divide)
            for (int i = 1; i < sz; i++)
            {
                if (compare(lst[i],pivot)<0)
                {
                    lSize++;
                } else
                {
                    rSize++;
                }
            }
            Tile[] left = new Tile[lSize];
            Tile[] right = new Tile[rSize];
            int lC = 0;
            int rC = 0;
            for (int i = 1; i < sz; i++)
            {
                if (compare(lst[i],pivot)<0)
                {
                    left[lC] = lst[i];
                    lC++;
                } else
                {
                    right[rC] = lst[i];
                    rC++;
                }
            }
            // sort sublists (conquor)  
            return qSortHelperAppend(quickSort(left), pivot, quickSort(right));
        }
    }
    private Tile[] qSortHelperAppend(Tile[] l, Tile p, Tile[] r)
    {
        Tile[] ret = new Tile[l.length + r.length + 1];
        int curr = 0;
        while (curr < l.length)
        {
            ret[curr] = l[curr];
            curr++;
        }
        ret[curr] = p;
        curr++;
        int c2 = 0;
        while (curr < l.length + 1 + r.length)
        {
            ret[curr] = r[c2];
            curr++;
            c2++;
        }
        return ret;
        // recursive
    }
    private boolean verifySort()
    {
        for (int i = 0; i < size-1; i++)
        {
            if (compare(i,i+1)>0)
            {
                return false;
            }
        }
        return true;
    }
    
    private int compare(int i, int j)
    {//return -1 if list[i] < list [j], 0 if list[i] == list [j], 1 if list[i] > list [j]
        if(list[i].x<list[j].x)
        {
            return -1;
        }
        else if(list[i].x == list[j].x)
        {
            if(list[i].y<list[j].y)
            {
                return -1;
            }
            else if(list[i].y==list[j].y)
            {
                return 0;
            }
            else return 1;
        }
        else return 1;
    }
    private int compare(Tile i, Tile j)
    {//return -1 if list[i] < list [j], 0 if list[i] == list [j], 1 if list[i] > list [j]
        if(i.x<j.x)
        {
            return -1;
        }
        else if(i.x == j.x)
        {
            if(i.y<j.y)
            {
                return -1;
            }
            else if(i.y==j.y)
            {
                return 0;
            }
            else return 1;
        }
        else return 1;
    }
    
    public void print()
    {
        for(int i = 0;i<size;i++)
        {
            System.out.println("Stile "+i+": ("+list[i].x/45+","+list[i].y/45+")");
        }
    }
}