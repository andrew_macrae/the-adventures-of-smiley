package smiley;

import java.awt.Image;

/**
 *
 * @author Andrew MacRae <macrae@berkeley.edu>
 */
public class TileImagePoint
{
    private Image im;
    public int gid;
    public int width,height;
    
    public TileImagePoint(Image img,int w, int h, int g)
    {
        width = w;
        height = h;
        gid = g;
        im = img;
    }
    
    public Image getImage()
    {
        return im;
    }
}
