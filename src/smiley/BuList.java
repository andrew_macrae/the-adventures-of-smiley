
package smiley;

import java.awt.Graphics;
import java.awt.Image;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 */
public class BuList
{
    private LinkedList<Projectile> list;
    int sz,max;
    private ListIterator<Projectile> iter;
    private Image projImg;
    private boolean fixedImage;
    
    public BuList(int sMx)
    {
        sz = 0;
        max = sMx;
        fixedImage = false;
        list = new LinkedList();     
    }
    public BuList(int sMx, Image im)
    {
        sz = 0;
        max = sMx;
        fixedImage = true;
        projImg = im;
        list = new LinkedList();        
    }
    
    public boolean add(Projectile p)
    {
        if(sz<max)
        {
            list.addLast(p);
            sz++;
            return true;
        }
        else return false;
    }
    
    public Projectile get()
    {
        return list.getLast();
    }
    public Projectile get(int idx)
    {
        return list.get(idx);
    }
    public boolean kill(int idx)
    {
        if(sz>0&&idx<sz)
        {
            list.remove(idx);
            sz--;
            return true;
        }
        else return false;
    }
    
    public void iterate(double d)
    {
        int toKill = -1; // This is a bit of a fudge ... kept getting ConcurrentModificationException exception due to iterator
        int counter = 0; // running on a separate thread. Instead never remove object on an iterator
        Projectile vomit;
        iter = list.listIterator();
        while(iter.hasNext())
        {
            vomit = iter.next();
            vomit.iterate(d);
            if(vomit.lifeTime <0)
            {
               toKill = counter; // mark the last occurance of a projectile to be removed. If more than one, we take care of one/cycle.
            }
            counter++;
        }
        if(toKill>=0) kill(toKill);       
    }
    
    public boolean checkIsIn(Squob s, double dx, double dy)
    {
        iter = list.listIterator();
        while(iter.hasNext())
        {
            if(s.isIn(iter.next(),dx,dy)) return true;
        }
        return false;
    }
    
    public void drawAll(Graphics g,double x0,double y0)
    {
        if(fixedImage)
        {
            Projectile tmp;
        
            iter = list.listIterator();
            while(iter.hasNext())
            {
                tmp = iter.next();
                g.drawImage(projImg, (int)(tmp.x-x0), (int)(tmp.y-y0),(int)tmp.w,(int)tmp.h, null);
            }        
        }
    }
}