package smiley;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class Boss extends Enemy
{

    private SpriteManager sMan_right,sMan_left, sMan_wait, sMan_spew,sMan_wakeup,sMan_dead,sMan_sleep;
    public int tickCounter = 100;
    private double w0,h0;    
    private Image webImg;
    private int webH,webW,webVelX,webVelY;

    public Boss(double xs, double ys, double ws, double hs, String sPath, double gr)
    {        
        super(xs, ys, ws, hs, gr);
        w0 = ws;
        h0 = hs;
        alive = true;
        type = 'B';
        vx = 0;
        state = 'a';
        idStr = "Spider Boss";
        
        webW = 20;
        webH = 75;
        webVelX = -20;
        webVelY = 0;
        
        String[] fN_d =
        {
            sPath + "boss_dead.png"
        };

        sMan_right = new SpriteManager(sPath + "spider_walk_right.png", 80, 45, 4, true);
        sMan_left = new SpriteManager(sPath + "spider_walk_left.png", 80, 45, 4, true);
        sMan_wait = new SpriteManager(sPath + "spider_wait.png", 80, 45, 4, true);
        sMan_spew = new SpriteManager(sPath + "spider_stand.png", 75, 75, 4, true);
        sMan_wakeup = new SpriteManager(sPath + "spider_wake.png", 80, 45, 7, false);
        sMan_dead = new SpriteManager(sPath + "spider_dead.png", 80, 45, 6, true);
        sMan_sleep = new SpriteManager(sPath + "spider_sleep.png", 80, 45, 1, false);
        
        //sMan_w = new SpriteManager(fN_w);
        //sMan_dead = new SpriteManager(fN_d);
        try
        {
            webImg  = ImageIO.read(new File("Images/Sprites/shooting_web.png"));
        } catch (IOException ex)
        {
            Logger.getLogger(Enemy.class.getName()).log(Level.SEVERE, null, ex);
        }
        bList = new BuList(3,webImg);
        hitPoints = maxHitPoints = 7;
        
        jumper = false;
    }

    public boolean addImage(String fN)
    {
        return false;
    }

    public void draw(Graphics g, double x0, double y0)
    {
        bList.drawAll(g, x0, y0);
        switch (state)
        {
            case 'a': // asleep
                g.drawImage(sMan_wait.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
                break;
            case 'u': // wake up
                g.drawImage(sMan_wakeup.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null); 
                break;
            case 'w': // wait
                g.drawImage(sMan_wait.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
                break;
            case 'c': // charge
                g.drawImage(sMan_left.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
                break;
            case 'p': // pre-charge
                g.drawImage(sMan_left.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
                break;
            case 's':// spew
                g.drawImage(sMan_spew.get(), (int) (x - x0), (int) (y - y0-h/1.8), (int) w*75/80, (int) h*75/45, null);
                break;
            case 'r':// retread
                g.drawImage(sMan_left.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
                break;
            case 'd': // dead
                g.drawImage(sMan_dead.get(), (int) (x - x0), (int) (y - y0), (int) w, (int) h, null);
                break;
        }
    }

    public void iterate(double d, boolean collH, boolean collV, boolean willFall, boolean onSlope, double dVX, double dVY)
    {
        super.iterate(d, collH, collV, willFall, onSlope, dVX, dVY);
        bList.iterate(d);
        if (isAlive())
        {
            if (state == 'r')
            {
                sMan_left.iterate(2 * d);
                if(vx<0)
                {
                    vx = 0;
                    state = 'w';
                    tickCounter = 240;
                    System.out.println("Waiting");
                }
            }
            else if (state == 'w')
            {
                sMan_wait.iterate(d);
                tickCounter--;
                if(tickCounter<0)
                {
                    state = 's';
                    tickCounter = 150;
                    System.out.println("Spewing");
                }
            }
            else if (state == 's')
            {
                sMan_spew.iterate(d);
                tickCounter--;
                if(tickCounter == 125||tickCounter == 25)
                {
                    bList.add(new Web(x+.2*w,y-0.3*h,webW,webH,webVelX,webVelY,0));
                    MainPanel.clips.SPIDER_SPEW.play();
                   
                }
                if(tickCounter<0)
                {
                    state = 'p';
                    tickCounter = 90;
                    System.out.println("Preparing");
                }               
            }
            else if (state == 'p')
            {
                sMan_wait.iterate(d);
                tickCounter--;
                if(tickCounter<0)
                {
                    state = 'c';
                    vx = -5;
                    System.out.println("Charging");
                    MainPanel.clips.SPIDER_HISS.play();
                }               
            }
            else if (state == 'c')
            {
                sMan_left.iterate(5*d);
                if(vx>0)
                {
                    state = 'r';
                    vx = 1;
                    System.out.println("Retreating");
                }               
            }            
                     
            else if (state == 'u')
            {
                sMan_wakeup.iterate(d);
            }
        } 
        else
        {
            sMan_dead.iterate(2 * d);
        }
    }

    public void kill()
    {
        hitPoints--;
        if (hitPoints < 0)
        {
            type = 'b';
            alive = false;
            state = 'd';
        }
    }

    public boolean setState(char c)
    {
        if (c == 'w')
        {
            state = 'w';
            return true;
        } else if (c == 's')
        {
            state = 's';
            return true;
        } else if (c == 'd')
        {
            state = 'd';
            return true;
        } else
        {
            return false;
        }
    }

    public char getState()
    {
        return state;
    }

    public boolean isAlive()
    {
        return alive;
    }
    
    class Web extends Projectile
    {
     public Web(double xs, double ys, double ws, double hs,double vvx, double vvy,double gr)
        {
            super(xs,ys,ws,hs);
            lifeTime = 200;
            vx =vvx;
            vy = vvy;
            grav = gr;
        }
        
        @Override
        public void iterate(double d)
        {
            x+=d*this.vx;
            y+=d*vy;
            //this.vy+=grav*d;
            
            lifeTime--;
        }
 
    }
}