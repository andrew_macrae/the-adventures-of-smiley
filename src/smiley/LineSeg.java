/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package smiley;

/**
 *
 * @author amacrae
 */
public class LineSeg
{
    public double x1,x2,y1,y2;
    
    public LineSeg(V2 p1, V2 p2) // x1,y1 are always the minimum
    {
        x1 = p1.x;
        x2 = p2.x;
        y1 = p1.y;
        y2 = p2.y;
    }
    public LineSeg(double tx1,double ty1,double tx2,double ty2)
    {
        x1 = tx1;
        x2 = tx2;
        y1 = ty1;
        y2 = ty2;
    }
    public boolean isVert()
    {
        if(x1==x2) return true;
        else return false;
    }
    public boolean isHoriz()
    {
        if(y1==y2) return true;
        else return false;
    }
    public double length()
    {
        return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    }
    public double height()
    {
        return y2-y1;
    }
    public double width()
    {
        return x2-x1;
    }
    public void displace(double d1,double d2)
    {
        x1+=d1;x2+=d1;
        y1+=d2;y2+=d2;
    }
    
    public void set(double tx1,double ty1,double tx2,double ty2)
    {
        x1 = tx1;
        x2 = tx2;
        y1 = ty1;
        y2 = ty2;
    }
}