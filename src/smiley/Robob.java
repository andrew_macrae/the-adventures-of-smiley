package smiley;

import java.awt.Graphics;

public class Robob extends Enemy
{

    private SpriteManager sMan_r, sMan_l, sMan_d, sMan_D;
    public char state;

    public Robob(double xs, double ys, double ws, double hs, String sPath, double g)
    {
        super(xs, ys, ws, hs,g);
        
        bList = new BuList(5);
        fireCounter = 0;
        fireDeadTime = 50;
        alive = true;
        type = 'r';
        dCy=-4;

        sMan_r = new SpriteManager(sPath + "robob_right.png", 40, 30, 4, true);
        sMan_l = new SpriteManager(sPath + "robob_left.png", 40, 30, 4, true);
        sMan_d = new SpriteManager(sPath + "robob_right_death.png", 40, 30, 8, false);
        sMan_D= new SpriteManager(sPath + "robob_left_death.png", 40, 30, 8, false);
        sMan_d.setDelay(8);
        sMan_D.setDelay(8);
        state = 'r';
        hitPoints = maxHitPoints= 5;
        hpCnt = 60;
    }

    public boolean addImage(String fN)
    {

        return false;
    }

    public void draw(Graphics g, double x0, double y0)
    {
        int dy = 4;
        switch (state)
        {
            case 'r':
                g.drawImage(sMan_r.get(), (int) (x - x0), (int) (y - y0+dy), (int) w, (int) h, null);
                break;
            case 'l':
                g.drawImage(sMan_l.get(), (int) (x - x0), (int) (y - y0+dy), (int) w, (int) h, null);
                break;
            case 'd':
                g.drawImage(sMan_d.get(), (int) (x - x0), (int) (y - y0+dy), (int) w, (int) h, null);
                break;
            case 'D':
                g.drawImage(sMan_D.get(), (int) (x - x0), (int) (y - y0+dy), (int) w, (int) h, null);
                break;
        }
        drawHP(g,(int)x0,(int)y0);
        for(int i = 0;i<bList.sz;i++)
        {
            bList.get(i).draw(g,x0,y0);
        }
    }

    public void iterate(double d, boolean collH, boolean collV, boolean willFall, boolean onSlope, double dVX, double dVY)
    {
        super.iterate(d, collH, collV, willFall, onSlope, dVX, dVY);
        if(fireCounter>0) fireCounter--; 
        bList.iterate(d);
        
        if (isAlive())
        {
        if(playerVicinity<250 && (x-playerX)/vx<0)
        {
            fire();
        }
            if (vx >= 0)
            {
                state = 'r';
            } else
            {
                state = 'l';
            }
        }
        switch (state)
        {
            case 'r':
                sMan_r.iterate(d);
                break;
            case 'l':
                sMan_l.iterate(d);
                break;
            case 'd':
                sMan_d.iterate(d);
                break;
            case 'D':
                sMan_D.iterate(d);
                break;
        }
    }

    public boolean setState(char c)
    {
        if (c == 'l')
        {
            state = 'l';
            return true;
        } else if (c == 'r')
        {
            state = 'r';
            return true;
        } else
        {
            return false;
        }
    }

    public void kill()
    {
        hitPoints--;
        if(hitPoints<=0)
        {
            alive = false;
            if(state == 'r')
            {
                state = 'd';
            }
            else state = 'D';
        }
        else
        {
            hpCounter = hpCnt;
        }
    }
    
    private void fire()
    {
        if(fireCounter<1)
        {
            bList.add(new CannonBall(x,y,10,10));
            fireCounter = fireDeadTime;
        }
    }
    
    class CannonBall extends Projectile
    {
        public CannonBall(double xs, double ys, double ws, double hs)
        {
            super(xs,ys,ws,hs);
            lifeTime = 200;
            this.vx = 10*sign(playerX-x);
            vy = 0;
        }
        
        @Override
        public void iterate(double d)
        {
            x+=d*this.vx;
            y+=d*vy;
            //this.vy+=d/2;
            
            lifeTime--;
        }
        @Override
        public void draw(Graphics g, double x0, double y0)
        {
            g.fillOval((int)(x-x0),(int)(y-y0),(int)w,(int)h);
        }
        
    }
}
