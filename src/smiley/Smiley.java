package smiley;

/**
 * @author Andrew MacRae <macrae@berkeley.edu>
 * */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
//import java.io.File;

public class Smiley extends JFrame implements KeyListener
{    
    MainPanel gPanel = new MainPanel();

    public void init()
    {        
        gPanel.setFocusable(true);
        gPanel.addKeyListener(this);

    }

    public Smiley()
    {
//Settings for frame
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900, 610);
        setTitle("Test");
        Container content = getContentPane();

        content.add(gPanel, BorderLayout.CENTER);
    }
// Action Event Handlers

    public void keyPressed(KeyEvent k)
    {
        gPanel.handleInput('p',k);
    }

    public void keyReleased(KeyEvent k)
    {
        gPanel.handleInput('r',k);
    }

    public void keyTyped(KeyEvent k)
    {
        gPanel.handleInput('t',k);
        repaint();       
    }
    
    public static void main(String[] args)
    {
        Smiley mf = new Smiley();
        mf.setVisible(true);
        mf.init();
    }
}