/*
 *  This software is free.
 */

package smiley;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 *
 */
public class Projectile extends Squob 
{
    public int lifeTime;
    public double vx,vy;
    public Projectile(double xs, double ys, double ws, double hs)
    {
        super(xs,ys,ws,hs);
        lifeTime  = 10;
        vx = vy = 0;
    }
    public void iterate(double d){;}
}
