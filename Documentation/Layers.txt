Layer guide:

Tileset:
- All sprites are on a sheet titled "Sprites"
- Currently, only one main collision tileset is supported
- Tilesheet corresponding to main Tile layer must start with prefix "fg_"

Layers:

fg2 (z = 1/2): Not used yet. Things halfway between the camera and the player
fg1 (z = 1): Scenery just in front of sprites
spriteLayer (z = 1): (COLL) Smiley, Enemies, etc
tiles (z = 1): (COLL) Actual object that elements collide with (ground, walls, etc)
spikes (z = 1): (COLL) Things that kill smiley by thouching them
bg1 (z = 1): Scenery just behind sprites
bg2 (z = 2): Nearby clouds trees
bg3 (z = 4): Distant clouds, mountains, etc.
bg4 (z = inf): The sun, sky, etc.