Current bug due to inefficient loading/storage of Images. Bandaid fix: during load screen, load all tiles once. I think a smarter fix is to have the image manager as before, but now each Tile doesn't get an image, but a key. Draw() will just return this key which is then drawn from thr ImageManager().

- tried creating hash table. Problem: suppose we run: 
im1 = new Image("test.png");
im2 = new Image("test.png");
these are still separate images and separate hashes are created. im1.equals(im2) returns false here.
Conclusion: use fileName instead of Image.hashCode();

	So now, Tile contains a string which points to the fileName();

Smiley
- smooth camera motion
- Level 1 aux artwork
- Level 2 design
- Level 2 aux artwork
- Level 3 design
- Level 3 aux artwork
- Level 4 design
- Level 4 aux artwork
- Level 5 tileset
- Level 5 design
- Level 5 aux artwork
- New enemies
- New Bosses
- Splash screens
- Look'n'feel shite-list
- Look'n'feel implement
- Mechanics Shite-list

		_-_-_-_-_-_-_ 		Known Bugs		_-_-_-_-_-_-_
- Projectiles hang in mid-air
- reset net after passing/dying on boss
- reset sky after dying on Boss
- player hit detection is not accurate when jumping on enemies with non-zero y-velocity
_-_-_-_-_-_- 	 Mechanics Wish List 	_-_-_-_-_-_-
- When Falling, always set state to falling
- fall a bit faster
- if landing, different ehaviour based on whther vy > vy_max

_-_-_-_-_-_- 	Look'n'Feel Wish List	 _-_-_-_-_-_-
- Fix midi issue (new drivers, import to mp3, or design new songs)
- low pass filer the camera following velocity
- boss bust
